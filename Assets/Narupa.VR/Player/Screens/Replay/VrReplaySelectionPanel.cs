﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using Narupa.VR.Player.Control;
using NSB.Utility;
using UnityEngine;

namespace Narupa.VR.Player.Screens.Replay
{
    /// <summary>
    /// Represents a replay.
    /// </summary>
    public class ReplayInfo
    {
        /// <summary>
        /// The trajectory bytes asset.
        /// </summary>
        public TextAsset Asset;

        //@hack to enable arbitrary scale factors when replaying multiplayer demos. @review
        /// <summary>
        /// The scale to play the trajectory back at.
        /// </summary>
        public float Scale = 1f;
    }

    public class VrReplaySelectionPanel : MonoBehaviour
    {
        [SerializeField] private VrAppController controller;

        private InstancePool<ReplayInfo> replays;

        [SerializeField] private InstancePoolSetup replaysSetup;

        [SerializeField] private List<TextAsset> trajectoryAssets = new List<TextAsset>();

        [SerializeField] private List<float> trajectoryScaleFactors = new List<float>();

        public void SelectReplay(ReplayInfo info)
        {
            controller.EnterRecording(info.Asset);
        }

        private void Awake()
        {
            replays = replaysSetup.Finalise<ReplayInfo>(true);
            controller = FindObjectOfType<VrAppController>();
        }

        private void OnEnable()
        {
            var replayInfo = new List<ReplayInfo>();
            //@review this is a god-awful hack because i can't listen to multiplayer scale changes on replays.
            for (var i = 0; i < trajectoryAssets.Count; i++)
            {
                var info = new ReplayInfo();
                info.Asset = trajectoryAssets[i];
                if (i < trajectoryScaleFactors.Count)
                    info.Scale = trajectoryScaleFactors[i];
                replayInfo.Add(info);
            }

            replays.SetActive(replayInfo);
        }
    }
}