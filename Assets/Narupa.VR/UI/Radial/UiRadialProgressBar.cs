﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.VR.UI.Primitive;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace Narupa.VR.UI.Radial
{
	/// <summary>
	/// 	Uses two UICircle's to draw a radial progress bar
	/// </summary>
	public class UiRadialProgressBar : UIBehaviour
	{

		[FormerlySerializedAs("backgroundBar")] public UiCircle BackgroundBar;
		[FormerlySerializedAs("progressBar")] public UiCircle ProgressBar;

		[Range(0, 1)]
		[SerializeField] private float _value;

		public float value
		{
			get { return _value; }
			set { SetValue(value); }
		}

		public void SetValue(float value)
		{
			this._value = value;
			ProgressBar.FillAmount = BackgroundBar.FillAmount * value;
			ProgressBar.SetVerticesDirty();
		}

		#if UNITY_EDITOR
		protected override void OnValidate()
		{
			base.OnValidate();
			SetValue(this._value);
		}
		#endif
	}
}
