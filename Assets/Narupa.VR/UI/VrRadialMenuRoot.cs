﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;

namespace Narupa.VR.UI
{
    /// <summary>
    ///     Placeholder class for finding the radial menu root in the heirarchy.
    /// </summary>
    public class VrRadialMenuRoot : MonoBehaviour
    {
    }
}