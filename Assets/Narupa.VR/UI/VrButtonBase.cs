﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections;
using Narupa.VR.UI.Tooltips;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Narupa.VR.UI
{
    /// <summary>
    ///     Base class for implementing a button in VR.
    /// </summary>
    public class VrButtonBase : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public static float HapticFeedBackStrength = 0.5f;

        protected Button Button;

        [SerializeField] protected string Message;

        [SerializeField] [Tooltip("Where to display tooltips for this button")]
        private MessageTooltipManager messageTooltip;

        [Header("Tooltip Options")] [SerializeField]
        protected string Title;

        private readonly float tooltipDelayTime = 0.2f;
        private uint toolTipId;

        public void OnPointerEnter(PointerEventData eventData)
        {
            StopCoroutine(StartTooltipDelay());
            StartCoroutine(StartTooltipDelay());
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            DestroyTooltip();
        }

        /// <summary>
        ///     Handler for when trigger is pulled while highlighted i.e. the button is clicked.
        /// </summary>
        public event EventHandler ButtonClicked;

        protected virtual void Awake()
        {
            Button = GetComponentInChildren<Button>();
            Button.onClick.AddListener(() => OnClick());

            if (messageTooltip == null) messageTooltip = GetComponentInParent<MessageTooltipManager>();
            if (messageTooltip == null)
                Debug.LogWarning(
                    "Unable to find tooltip manager in parent for this button, tooltips will not be shown.");
        }

        protected virtual void OnClick(bool userActivated = true)
        {
            if (ButtonClicked != null) ButtonClicked(this, null);
        }

        private IEnumerator StartTooltipDelay()
        {
            yield return new WaitForSeconds(tooltipDelayTime);
            if (!string.IsNullOrEmpty(Title) && messageTooltip != null)
                toolTipId = messageTooltip.DisplayTooltip(transform, Title, Message);
        }

        private void DestroyTooltip()
        {
            StopCoroutine(StartTooltipDelay());
            if(messageTooltip != null) messageTooltip.HideTooltip(toolTipId);
        }

        private void OnDisable()
        {
            DestroyTooltip();
        }
    }
}