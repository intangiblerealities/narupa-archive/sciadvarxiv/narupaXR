﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.VR.Player.Screens.Main;
using Narupa.VR.UI.Buttons;
using UnityEngine;

namespace Narupa.VR.UI
{
    /// <summary>
    ///     Class for handling press of the main menu toggle on the VR controller.
    /// </summary>
    public class VrMainMenuButton : MonoBehaviour
    {
        private bool foundVrCanvas;

        [SerializeField] private MainMenuController mainMenu;

        [SerializeField] private MainMenuCanvas mainMenuCanvas;

        protected void Awake()
        {
            GetComponent<UiButton>().OnButtonClick.AddListener((sender, args) => EnableMainMenu());
            
            if (mainMenu == null) mainMenu = FindObjectOfType<MainMenuController>();

            if (mainMenuCanvas == null) mainMenuCanvas = FindObjectOfType<MainMenuCanvas>();
            if (mainMenuCanvas == null)
            {
                gameObject.SetActive(false);
                foundVrCanvas = false;
                Debug.LogWarning("Failed to find main menu canvas, main menu will not be accessible from VR. ");
            }
            else
            {
                foundVrCanvas = true;
            }
        }

        private void DisableMainMenu()
        {
            if (foundVrCanvas == false) return;
            mainMenu.EnableMainMenu(false);
            mainMenuCanvas.gameObject.SetActive(false);
        }

        private void EnableMainMenu()
        {
            if (foundVrCanvas == false) return;
            mainMenu.EnableMainMenu(true);
            mainMenuCanvas.gameObject.SetActive(true);
        }
    }
}