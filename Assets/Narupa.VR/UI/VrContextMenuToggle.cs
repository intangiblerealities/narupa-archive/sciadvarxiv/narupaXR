﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using UnityEngine;

namespace Narupa.VR.UI
{
    /// <summary>
    ///     Class for toggling a context menu on or off.
    /// </summary>
    internal class VrContextMenuToggle : VrToggleBase
    {
        [SerializeField] private VrCanvas canvasToReturnTo;

        [SerializeField] private VrCanvas canvasToToggle;

        [SerializeField] private VrContextMenu contextMenu;

        private bool switchedToCanvas;

        protected override void Awake()
        {
            base.Awake();

            ToggleChangeValue += OnToggleChangeValue;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            Toggle.isOn = false;

            OnClick(Toggle.isOn, false);
        }

        /// <summary>
        ///     On toggle changing value, triggers the <see cref="VrContextMenu" /> to switch on or off the menu.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="e"></param>
        private void OnToggleChangeValue(object obj, EventArgs e = null)
        {
            var toggledOn = (bool) obj;

            if (toggledOn == false)
            {
                if (canvasToReturnTo != null && switchedToCanvas)
                {
                    switchedToCanvas = false;
                    contextMenu.SwitchToCanvas(canvasToReturnTo, canvasToToggle);
                }
                else
                {
                    StartCoroutine(canvasToToggle.DisableRoutine(canvasToToggle.AppearanceTime));
                }
            }
            else
            {
                switchedToCanvas = true;
                contextMenu.SwitchToCanvas(canvasToToggle, canvasToReturnTo);
            }
        }
    }
}