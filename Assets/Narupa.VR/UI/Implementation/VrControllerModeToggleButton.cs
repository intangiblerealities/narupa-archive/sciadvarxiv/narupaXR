﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.VR.Controller;
using Narupa.VR.UI.Buttons;
using UnityEngine;

namespace Narupa.VR.UI.Implementation
{
	/// <summary>
	/// 	Ensures that the controller button toggle is implemented
	/// </summary>
	[RequireComponent(typeof(UiToggleButton))]
	public class VrControllerModeToggleButton : MonoBehaviour
	{
		public ControllerMode Mode;
	}
}
