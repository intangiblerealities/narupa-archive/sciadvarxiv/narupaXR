﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.VR.Controller;
using Narupa.VR.Player.Control;
using UnityEngine;

namespace Narupa.VR.UI.Implementation
{
	public class VrControllerModeToggleGroup : MonoBehaviour
	{

		private VrPlaybackRenderer playbackRenderer;
		private VrControllerStateManager controllerStateManager;

		// Use this for initialization
		void Awake ()
		{
			playbackRenderer = FindObjectOfType<VrPlaybackRenderer>();
			playbackRenderer.TrajectoryPlaybackStarted += PlaybackRendererOnTrajectoryPlaybackStarted;
			playbackRenderer.SimulationStarted += PlaybackRendererOnSimulationStarted;
			controllerStateManager = FindObjectOfType<VrControllerStateManager>();
			controllerStateManager.ModeEnabled += ControllerStateManagerOnModeEnabled;
			controllerStateManager.ModeDisabled += ControllerStateManagerOnModeDisabled;
		}

		private void ControllerStateManagerOnModeDisabled(object sender, VrControllerStateManager.ControllerModeAlteredEventArgs e)
		{
			foreach (var toggle in this.GetComponentsInChildren<VrControllerModeToggleButton>(true))
			{
				if(toggle.Mode == e.Mode)
					toggle.gameObject.SetActive(false);
			}
		}

		private void ControllerStateManagerOnModeEnabled(object sender, VrControllerStateManager.ControllerModeAlteredEventArgs e)
		{
			foreach (var toggle in this.GetComponentsInChildren<VrControllerModeToggleButton>(true))
			{
				if(toggle.Mode == e.Mode)
					toggle.gameObject.SetActive(true);
			}
		}

		private void PlaybackRendererOnSimulationStarted(object sender, VrPlaybackRenderer.SimulationStartedEventArgs e)
		{
			controllerStateManager.EnableMode("Interaction");
			controllerStateManager.SetMode("Interaction");
			controllerStateManager.DisableMode("Playback");
		}

		private void PlaybackRendererOnTrajectoryPlaybackStarted(object sender, VrPlaybackRenderer.TrajectoryPlaybackStartedEventArgs e)
		{
			controllerStateManager.EnableMode("Playback");
			controllerStateManager.SetMode("Playback");
			controllerStateManager.DisableMode("Interaction");
		}

	}
}
