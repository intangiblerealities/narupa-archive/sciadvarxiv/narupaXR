﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using UnityEngine;
using Valve.VR;

namespace Narupa.VR.UI
{
    /// <summary>
    ///     Class for handling what happens when the user presses the menu button on the controller.
    /// </summary>
    public class VrMenuButtonController : MonoBehaviour
    {
        public static bool InMenu;

        public SteamVR_Action_Boolean actionMenu;
        private bool exitUiMode;

        [Tooltip("Debug Mode enabling access to the vr controller ui through keyboard press (M)")] [SerializeField]
        private bool keyboardDebugAccess;

        private MainMenuCanvas mainMenuCanvas;

        private bool menuAccessEnabled = true;

        /// <summary>
        ///     The model for the menu button, for animating.
        /// </summary>
        [SerializeField] private Transform menuButtonTransform;

        private Vector3 originalButtonPositon;

        private VrControllerUi vrControllerUi;

        private void Awake()
        {
        }

        // Use this for initialization
        private void Start()
        {
            if (actionMenu != null)
            {
                actionMenu.AddOnChangeListener(OnMenuButtonChanged, SteamVR_Input_Sources.LeftHand);
            }

            if (menuButtonTransform != null)
                originalButtonPositon = menuButtonTransform.localPosition;

            vrControllerUi = FindObjectOfType<VrControllerUi>();
            SwitchOutOfMenuMode();
            vrControllerUi.UiModeExited += OnUiModeExit;
            mainMenuCanvas = FindObjectOfType<MainMenuCanvas>();
        }
        
        private void OnMenuButtonChanged(SteamVR_Action_In action)
        {
            if (actionMenu.GetState(SteamVR_Input_Sources.LeftHand))
            {
                OnMenuButtonPressed();
                MenuButtonPressed?.Invoke();
            }
            else
            {
                OnMenuButtonReleased();
                MenuButtonReleased?.Invoke();
            }
        }

        private void OnMenuButtonReleased()
        {
            if (menuButtonTransform != null) menuButtonTransform.localPosition = originalButtonPositon;
        }

        public event Action MenuButtonPressed;
        public event Action MenuButtonReleased;

        private void OnUiModeExit(object sender, EventArgs e)
        {
            exitUiMode = true;
        }

        private void Update()
        {
            if (keyboardDebugAccess)
                if (Input.GetKeyDown(KeyCode.M))
                    OnMenuButtonPressed();

            if (exitUiMode)
            {
                InMenu = false;
                OnSwitchOutOfMenuMode();
            }

            if (exitUiMode) exitUiMode = false;
        }

        private void SwitchOutOfMenuMode()
        {
            vrControllerUi.HideUi();
            OnSwitchOutOfMenuMode();
        }

        private void OnSwitchOutOfMenuMode()
        {
            //if (radialMenuRoot != null) radialMenuRoot.gameObject.SetActive(true);
            //Leave pointer enabled...
            //pointer.enabled = false;
            //pointerRenderer.enabled = false;
        }

        private void SwitchIntoMenuMode()
        {
            //Leave radial menus enabled.
            //if (radialMenuRoot != null) radialMenuRoot.gameObject.SetActive(false);
            vrControllerUi.ShowUi();
            //pointer.enabled = true;
            //pointerRenderer.enabled = true;
        }

        private void OnMenuButtonPressed()
        {
            if (menuButtonTransform != null) menuButtonTransform.localPosition += new Vector3(0f, -0.0015f, 0f);
            if (menuAccessEnabled == false)
            {
                //If user somehow is in the menu without access to it, hide it.
                if (InMenu)
                {
                    InMenu = false;
                    SwitchOutOfMenuMode();
                }

                return;
            }

            InMenu = !InMenu;
            if (InMenu)
            {
                SwitchIntoMenuMode();
            }
            else
            {
                //If switching out of VR, disable the main menu in case the user activated it.
                if(mainMenuCanvas)
                    mainMenuCanvas.gameObject.SetActive(false);
                SwitchOutOfMenuMode();
            }
        }

        public void ShowVrMenu(bool showMenu)
        {
            InMenu = showMenu;
            exitUiMode = false;
            if (showMenu)
                SwitchIntoMenuMode();
            else SwitchOutOfMenuMode();
        }

        public void EnableMenuButton(bool b)
        {
            if (b)
            {
                if (menuButtonTransform != null) menuButtonTransform?.gameObject.SetActive(true);
                menuAccessEnabled = true;
            }
            else
            {
                if (menuButtonTransform != null) menuButtonTransform?.gameObject.SetActive(false);
                menuAccessEnabled = false;
            }
        }
    }
}