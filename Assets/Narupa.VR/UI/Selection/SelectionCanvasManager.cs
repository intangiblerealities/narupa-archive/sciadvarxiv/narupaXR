﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.VR.Controller;
using Narupa.VR.Interaction;
using Narupa.VR.Player.Control;
using Narupa.VR.Selection;
using TMPro;
using UnityEngine;

namespace Narupa.VR.UI.Selection
{
    /// <summary>
    ///     Class for managing the selection canvas
    /// </summary>
    /// <remarks>
    ///     Handles turning on and off of buttons, and switching to and from Selection Mode.
    /// </remarks>
    internal class SelectionCanvasManager : MonoBehaviour
    {
        private CreateSelectionButton createSelectionButton;

        [SerializeField] private TextMeshProUGUI currentModeText;

        [SerializeField] private TextMeshProUGUI currentSelectionText;
        private DeleteSelectionButton deleteSelectionButton;

        private readonly string noSelectionText = "Active Selection: None";

        [SerializeField] private VrContextMenu menuSwitcher; 
        
        [SerializeField] private VrCanvas selectionEditorCanvas;

        [SerializeField] private VrCanvas playModeCanvas; 
        
        private SelectionManager selectionManager;

        /// <summary>
        ///     Reference to the menu for switching between selections.
        /// </summary>
        public SelectionSwitchManager SelectionSwitchManager;

        private VrAtomSelectionManager[] vrAtomSelectionManagers;

        private VrControllerStateManager controllerStateManager;

        private void Awake()
        {
            controllerStateManager = FindObjectOfType<VrControllerStateManager>();
            controllerStateManager.ModeStart += ControllerStateManagerOnModeStart;
            //We use this to enable touch/click based selections.
            FindObjectOfType<VrAppController>();
            //Selection manager handles the actual details of selectiosn.
            selectionManager = FindObjectOfType<SelectionManager>();
            //TODO messy?
            SelectionSwitchManager = FindObjectOfType<SelectionSwitchManager>();
            if (SelectionSwitchManager == null)
                throw new NullReferenceException("Failed to find selection switcher in scene!");
            createSelectionButton = SelectionSwitchManager.GetComponentInChildren<CreateSelectionButton>();
            createSelectionButton.CreateSelectionClicked += OnCreateSelectionClicked;
            SelectionSwitchManager.SelectionHighlighted += OnSelectionHighlighted;
            SelectionSwitchManager.SelectionUnhighlighted += OnSelectionUnhighlighted;
            SelectionSwitchManager.SelectionChosen += OnSelectionChosen;
            //Selection managers handle pointing the controller at an atom and selecting it.
            vrAtomSelectionManagers = FindObjectsOfType<VrAtomSelectionManager>();
            //OnSelectionCleared();
            UiModeToggle.UiModeChange += OnUiModeChange;
        }

        private void OnUiModeChange(object sender, EventArgs e)
        {
            var mode = (UiModeToggle.UiMode) sender;

            switch (mode)
            {
                case UiModeToggle.UiMode.Novice:
                    selectionManager.ActiveSelection = null;
                    SelectionSwitchManager.HideSelectionManager();
                    controllerStateManager.SetModeDefault();
                    break;
                case UiModeToggle.UiMode.Advanced:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void OnCreateSelectionClicked(object sender, EventArgs e)
        {
            //Whenever a new selection is created, switch to selection edit mode.
            var selection = CreateNewSelection();
            selectionManager.SetAsCurrentSelection(selection);
            SetLabelForSelection(selection);
            controllerStateManager.SetMode("Selection");
        }

        public void OnSelectionCleared()
        {
            SetLabelForSelection(null);
            controllerStateManager.SetModeDefault();
        }

        private void OnSelectionChosen(InteractiveSelection selection, EventArgs e)
        {
            selectionManager.SetAsCurrentSelection(selection);
            SetLabelForSelection(selection);
        }

        private void SetLabelForSelection(InteractiveSelection selection)
        {
            if (selection != null)
                currentSelectionText.text = selection.Name;
            else
                currentSelectionText.text = noSelectionText;
        }

        private void OnSelectionUnhighlighted(InteractiveSelection selection, EventArgs e)
        {
            selectionManager.RestorePreviousSelection();
        }

        private void OnSelectionHighlighted(InteractiveSelection selection, EventArgs e)
        {
            selectionManager.SetAsCurrentSelection(selection);
        }

        private InteractiveSelection CreateNewSelection()
        {
            return selectionManager.CreateNewSelection("Selection " + (selectionManager.Selections.Count + 1));
        }

        /// <summary>
        ///     Switches the UI and controllers over to Selection Mode, in which a user can select atoms instead of interacting
        ///     with them.
        /// </summary>
        /// <param name="switchToSelectionMode"></param>
        private void ControllerStateManagerOnModeStart(object sender, VrControllerStateManager.ControllerModeChangedEventArgs e)
        {
            currentModeText.text = $"Mode: {e.NewMode.name}";

            var switchToSelectionMode = e.NewMode.name == "Selection";
            
            //If instructed to switch to selection edit mode and there isn't a selection, create one.
            if (e.NewMode.name == "Selection")
            {
                if (selectionManager.ActiveSelection == null)
                {
                    var selection = CreateNewSelection();
                    SetLabelForSelection(selection);
                }
                menuSwitcher.SwitchToCanvas(selectionEditorCanvas);
                selectionManager.ShowCurrentSelection(true);
            }
            else
            {
                menuSwitcher.SwitchToCanvas(playModeCanvas);
                selectionManager.ShowCurrentSelection(false);
            }

            //Set the atom selection routines running.
            foreach (var vrController in vrAtomSelectionManagers)
                vrController.SwitchToSelectionMode(switchToSelectionMode);

            //Enable touch based selection controls in the app controller.
            VrAppController.SelectionMode = switchToSelectionMode;

            //Disable interaction forces
            VrInteractionController.InteractionsDisabled = e.NewMode.name != "Interaction";

            var playback = FindObjectOfType<VrPlaybackRenderer>();
            if(switchToSelectionMode)
                playback.StopPlay();
            else
                playback.ResumePlay();

        }
    }
}