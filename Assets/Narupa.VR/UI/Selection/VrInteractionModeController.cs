﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.VR.Controller;
using UnityEngine;
using UnityEngine.UI;

namespace Narupa.VR.UI.Selection
{
    /// <summary>
    ///     Class for switching between Selection and Interaction mode.
    /// </summary>
    public class VrInteractionModeController : MonoBehaviour
    {
        
        //todo: replace with script that does this automatically
        [SerializeField] private Toggle interactionModeToggle;

        [SerializeField] private Toggle playbackModeToggle;

        [SerializeField] private Toggle selectionModeToggle;

        private VrControllerStateManager controllerStateManager;

        private void OnEnable()
        {
            controllerStateManager = FindObjectOfType<VrControllerStateManager>();
        }

        // Use this for initialization
        private void Start()
        {
            controllerStateManager.ModeStart += ControllerStateManagerOnModeStart;
        }

        private void ControllerStateManagerOnModeStart(object sender, VrControllerStateManager.ControllerModeChangedEventArgs e)
        {
            var newMode = e.NewMode;
            interactionModeToggle.isOn = newMode.name == "Interaction";
            playbackModeToggle.isOn = newMode.name == "Playback";
            selectionModeToggle.isOn = newMode.name == "Selection";
        }

    }
}