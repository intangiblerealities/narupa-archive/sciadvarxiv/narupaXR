﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Transport.Variables.Interaction;
using Narupa.VR.Selection;
using UnityEngine;

namespace Narupa.VR.UI.Selection
{
    public class LayerInteractionToggle : VrToggleBase
    {
        private SelectionManager selectionManager;

        [SerializeField] private InteractionType type;

        [SerializeField] private bool RestraintLayer = false;

        private RestraintManager restraintManager;

        private bool addRestraint;
        private bool removeRestraint;
        private bool restraintEnabled;

        // Use this for initialization
        private void Start()
        {
            Toggle.onValueChanged.AddListener(val => OnValueChange(val));
            selectionManager = FindObjectOfType<SelectionManager>();

            restraintManager = FindObjectOfType<RestraintManager>();
        }

        private void OnValueChange(bool arg0)
        {
            //If not enabled and this is a restraint layer, ensure restraint is disabled.
            if (!arg0)
            {
                if (RestraintLayer && restraintEnabled)
                {
                    addRestraint = false;
                    removeRestraint = true;
                }

                return;
            }

            if (selectionManager.ActiveSelection == null)
                ActiveSelectionTemplate.InteractWithBaseLayer = type == InteractionType.SingleAtom;
            else
            {
                selectionManager.ActiveSelection.InteractionProperties.InteractionType = type;
                selectionManager.ActiveSelection.InteractionProperties.RestraintLayer = RestraintLayer;
                if (RestraintLayer) addRestraint = true;
            }
        }

        private void Update()
        {
            //Handles adding and removing restraints based on button state.
            if (addRestraint)
            {
                restraintManager.AddRestraints(selectionManager.ActiveSelection);
                restraintEnabled = true;
                addRestraint = false;
                removeRestraint = false;
            }

            if (removeRestraint)
            {
                restraintManager.RemoveRestraints(selectionManager.ActiveSelection);
                restraintEnabled = false;
                addRestraint = false;
                removeRestraint = false;
            }
        }
    }
}
