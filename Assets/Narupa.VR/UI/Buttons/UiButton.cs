﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;

namespace Narupa.VR.UI.Buttons
{
	
	/// <summary>
	/// 	Exposes button related events, should be placed on any button on the UI
	/// </summary>
	[RequireComponent(typeof(UnityEngine.UI.Button))]
	public class UiButton : UiButtonBase {

		public ButtonEvent OnButtonClick;

		protected override  void Awake()
		{
			base.Awake();
			button.onClick.AddListener(OnClick);
		}

		public void OnClick()
		{
			OnButtonClick?.Invoke(this, new ButtonEventArgs(button));
		}

	}
}
