﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.IO;
using Nano.Client;
using Nano.Transport.Variables.Interaction;
using Narupa.Client.Network;
using Narupa.Client.Network.Event;
using Narupa.VR.Player.Control;
using Narupa.VR.Selection.Rendering;
using Narupa.VR.UI.RenderModes;
using Narupa.VR.UI.Selection;
using Newtonsoft.Json;
using NSB.MMD;
using NSB.MMD.Base;
using NSB.MMD.RenderingTypes;
using NSB.Processing;
using NSB.Simbox.Topology;
using UnityEngine;

namespace Narupa.VR.Selection
{   
    public class SelectionsSerializer : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var manager = value as SelectionManager;
            if (manager == null)
                return;
            List<InteractiveSelectionInfo> selectionInfos = new List<InteractiveSelectionInfo>();
            InteractiveSelectionInfo selectionInfoBase = new InteractiveSelectionInfo()
            {
                Name = "base",
                Style = manager.BaseLayerRendererManager.Style.Name.ToLower(),
                InteractionProperties = new InteractionProperties()
                {
                    InteractionType = ActiveSelectionTemplate.InteractWithBaseLayer
                        ? InteractionType.SingleAtom
                        : InteractionType.NoInteraction,
                    RestraintLayer = false
                },
                Renderer = manager.BaseLayerRendererManager.ActiveRenderer.name.ToLower(),
                IsVisible = ActiveSelectionTemplate.BaseLayerVisible
            };
            selectionInfos.Add(selectionInfoBase);
            foreach (var selection in manager.Selections)
            {
                InteractiveSelectionInfo info = new InteractiveSelectionInfo(selection);
                selectionInfos.Add(info);
            }
            serializer.Serialize(writer, selectionInfos);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return serializer.Deserialize<List<InteractiveSelectionInfo>>(reader);
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(SelectionManager).IsAssignableFrom(objectType);
        }
    }
    
    /// <summary>
    ///     Manages the playback and rendering of selections.
    /// </summary>
    [JsonConverter(typeof(SelectionsSerializer))]
    public class SelectionManager : MonoBehaviour
    {
        /// <summary>
        ///     The active selection.
        /// </summary>
        [HideInInspector] public InteractiveSelection ActiveSelection;

        public RendererManager BaseLayerRendererManager;

        public bool SelectionPathSet => !string.IsNullOrEmpty(selectionPath);
        
        private bool newSimulation;

        private TransparentSelectionHighlighter currentSelectionHighlighter;

        private bool initialised;

        private SelectionHighlighter possibleSelectionHighlighter;

        private InteractiveSelection previousSelection;

        private VrPlaybackRenderer playbackRenderer;
        private NetworkManager networkManager;

        private string selectionPath;
        [SerializeField]
        //TODO styles need to be known about globally. 
        private StyleSource[] styleSources; 


        
        /// <summary>
        ///     The object to place spawned selection renderers.
        /// </summary>
        [SerializeField] private GameObject rootObjectForSelectionRendering;

        /// <summary>
        ///     The object to place spawned <see cref="InteractiveSelection" />
        /// </summary>
        [SerializeField] private GameObject rootObjectForSelections;

        /// <summary>
        ///     Event triggered when a new seleciton is created.
        /// </summary>
        public EventHandler SelectionCreated;

        /// <summary>
        ///     The game object where the renderer for selection editing lives.
        ///     TODO Get this programmatically?
        /// </summary>
        [SerializeField] private GameObject selectionEditingRendererRoot;

        [SerializeField] private InteractiveSelection selectionPrefab;

        /// <summary>
        ///     List of selections currently in the scene.
        /// </summary>
        [HideInInspector] public List<InteractiveSelection> Selections = new List<InteractiveSelection>();

        private Dictionary<string, NSBStyle> styleNameLookup;
        private bool simulationNameReceived;
        private string simulationName;

        private bool selectionDataReceived = false;
        private string selectionData;

        /// <summary>
        ///     Event triggered when the a selection becomes the active selection.
        /// </summary>
        public event EventHandler ActiveSelectionChanged;

        /// <summary>
        ///     Creates a new selection with the given name, and sets it as the active layer.
        /// </summary>
        /// <param name="selectionName"></param>
        /// <param name="silent"> If set to <c> true </c>, the selection will be created without it becoming visible or active. </param>
        /// <returns> The newly created <see cref="InteractiveSelection" />. </returns>
        public InteractiveSelection CreateNewSelection(string selectionName, bool silent = false)
        {
            var newSelection = Instantiate(selectionPrefab, rootObjectForSelections.transform);
            newSelection.Name = selectionName;
            newSelection.name = selectionName;
            var renderingRoot = newSelection.RendererRoot;
            renderingRoot.transform.SetParent(rootObjectForSelectionRendering.transform, false);
            renderingRoot.name = selectionName;
            renderingRoot.transform.localPosition = Vector3.zero;
            if(!silent) renderingRoot.gameObject.SetActive(true);
            //Bit of a hack to get selection colors more obvious.
            var cpkRepresentation = renderingRoot.GetComponentInChildren<CPKRepresentation>();
            if (cpkRepresentation != null)
            {
                cpkRepresentation.Settings.AtomScaling.Value = 0.45f;
                cpkRepresentation.Settings.BondRadius.Value = 0.045f;
            }

            newSelection.gameObject.SetActive(true);
            Selections.Add(newSelection);
            if(!silent) SetAsCurrentSelection(newSelection);
            if(!silent) SelectionCreated?.Invoke(newSelection, null);
            return newSelection;
        }


        public void SaveSelections(string path)
        {
            string selectionsJson = JsonConvert.SerializeObject(this, Formatting.Indented);


            string directory = Path.GetDirectoryName(path);
            if (directory != null)
                System.IO.Directory.CreateDirectory(directory);
            
            using (System.IO.StreamWriter file = 
                new System.IO.StreamWriter(path))
            {
                file.Write(selectionsJson);
            }

            selectionPath = path;
        }

        public void SaveSelections()
        {
            if (SelectionPathSet == false)
            {
                throw new NullReferenceException("Path to save selections has not been set!");
            }
            SaveSelections(selectionPath);
        }

        public void LoadSelections(string jsonData, bool fromFile = false)
        {
            string json;
            
            if (fromFile)
            {
                using (System.IO.StreamReader reader = new StreamReader(jsonData))
                {
                    json = reader.ReadToEnd();
                }
            }
            else json = jsonData;

            var newSelections = LoadSelectionsFromJson(json);

            //Current error handling is to simply do nothing if an invalid .json is passed. Is noted in the editor debug log.
            if (newSelections.Count == 0)
            {
                //Code to notify user has handed an invalid file here?
                return;
            }

            ClearSelections();
            SetupStyleKeys();

            foreach (var selectionInfo in newSelections)
            {
                //Check whether or not user wants a custom color 
                string sInfo = selectionInfo.Style.Trim();

                NSBStyle style;
                if (IsHexidecimal(sInfo))
                {
                    Color newColor;
                    ColorUtility.TryParseHtmlString(sInfo, out newColor);
                    style = FindObjectOfType<ColorStyleGenerator>().InitializeColor(newColor);
                }
                else
                {
                    style = TryGetStyle(sInfo);
                }

                if (selectionInfo.Name == "base")
                {
                    BaseLayerRendererManager.SetRenderStyle(style);
                    BaseLayerRendererManager.SetRenderer(selectionInfo.Renderer);
                    ActiveSelectionTemplate.InteractWithBaseLayer =
                        selectionInfo.InteractionProperties.InteractionType == InteractionType.SingleAtom;
                    BaseLayerRendererManager.gameObject.SetActive(selectionInfo.IsVisible);
                }
                else
                {
                    InteractiveSelection selection;
                    try
                    { 
                        selection = CreateNewSelection(selectionInfo.Name, true);
                    }
                    catch(Exception e)
                    {
                        Debug.LogException(e, gameObject);
                        continue;
                    } 
                    
                    selection.InteractionProperties = selectionInfo.InteractionProperties;
                    selection.RendererRoot.Initialise();
                    selection.RendererRoot.SetRenderStyle(style);
                    selection.RendererRoot.SetRenderer(selectionInfo.Renderer);
                    selection.Selection.AddRange(selectionInfo.SelectedAtoms);
                    selection.RendererRoot.gameObject.SetActive(selectionInfo.IsVisible);
                }
            }
            SetAsCurrentSelection(null);     
        }

        private bool IsHexidecimal(string hex)
        {
            hex = hex.Trim();
            //If doesn't start with # or is not 7 characters long, not hexidecimal
            if (hex[0] != '#' || hex.Length != 7)
            {
                return false;
            }

            for (int i = 1; i < hex.Length; i++)
            {
                //If a character in given found to not be hexadecimal
                if (!((hex[i] >= '0' && hex[i] <= '9') ||
                      (hex[i] >= 'a' && hex[i] <= 'f') ||
                      (hex[i] >= 'A' && hex[i] <= 'F')))
                {
                    return false;
                }
            }
            return true;
        }

        private NSBStyle TryGetStyle(string styleName)
        {
            if (styleNameLookup == null || styleNameLookup.Count == 0)
                throw new NullReferenceException("Style Name Lookup not initialised!");
            NSBStyle style;
            bool success = styleNameLookup.TryGetValue(styleName, out style);
            if (success == false)
            {
                style = styleSources[0].Component.Style;
            }
            return style;
        }

        private void SetupStyleKeys()
        {
            styleNameLookup = new Dictionary<string, NSBStyle>();
            foreach (var styleSource in styleSources)
            {
                styleNameLookup[styleSource.Component.Style.Name.ToLower()] = styleSource.Component.Style;
            }

            ColorStyleGenerator colorGenerator = FindObjectOfType<ColorStyleGenerator>();
            foreach (var colorStyle in colorGenerator.ColorStyles)
            {
                styleNameLookup[colorStyle.Style.Name.ToLower()] = colorStyle.Style;
            }
            
        }
        
        internal List<InteractiveSelectionInfo> LoadSelectionsFromJson(string json)
        {
            List<InteractiveSelectionInfo> selectionInfo = new List<InteractiveSelectionInfo>();

            //Check that a valid json string has been passed
            json = json.Trim();
            if (string.IsNullOrWhiteSpace(json) || 
                !((json.StartsWith("{") && json.EndsWith("}")) || (json.StartsWith("[") && json.EndsWith("]"))))
            {
                return selectionInfo;
            }

            try
            {
                selectionInfo = JsonConvert.DeserializeObject<List<InteractiveSelectionInfo>>(json);
            }
            catch
            {
                Debug.Log("Error reading in json file");
                return selectionInfo;
            }
            return selectionInfo;
        }
        
        /// <summary>
        ///     Sets the selection in the given index as the active selection and displays it.
        /// </summary>
        /// <param name="index"> Index of the selection in this instance's list of selections."/></param>
        public void SetAsCurrentSelection(int index)
        {
            if (index > 0 && index < Selections.Count) SetAsCurrentSelection(Selections[index]);
        }

        /// <summary>
        ///     Sets the given selection as the active selection and displays it.
        /// </summary>
        /// <param name="selection"></param>
        public void SetAsCurrentSelection(InteractiveSelection selection)
        {
            previousSelection = ActiveSelection;
            ActiveSelection = selection;
            ActiveSelectionChanged?.Invoke(selection, null);
            if (selection == null)
            {
                ShowCurrentSelection(false);
                return;
            }

            selection.SelectionHighlighter = possibleSelectionHighlighter;
            currentSelectionHighlighter.ActiveSelection = selection.Selection;
            ShowCurrentSelection(true);
        }

        /// <summary>
        ///     Displays the current selection.
        /// </summary>
        /// <param name="enable"></param>
        public void ShowCurrentSelection(bool enable)
        {
            if (initialised == false) return;
            currentSelectionHighlighter.gameObject.SetActive(enable);
            possibleSelectionHighlighter.Clear();
            if (enable && ActiveSelection != null)
                selectionEditingRendererRoot.gameObject.SetActive(true);
            else
                selectionEditingRendererRoot.gameObject.SetActive(false);
        }

        /// <summary>
        ///     Deletes the currently active simulation.
        /// </summary>
        internal void DeleteActiveSelection()
        {
            DeleteSelection(ActiveSelection);
        }

        /// <summary>
        ///     Deletes the specified <see cref="InteractiveSelection" />.
        /// </summary>
        /// <param name="interactiveSelection"></param>
        internal void DeleteSelection(InteractiveSelection interactiveSelection)
        {
            if (interactiveSelection == ActiveSelection) ShowCurrentSelection(false);
            Selections.Remove(interactiveSelection);
            Destroy(interactiveSelection.RendererRoot.gameObject);
            Destroy(interactiveSelection.gameObject);

            if (interactiveSelection == ActiveSelection) SetAsCurrentSelection(null);
        }

        /// <summary>
        ///     Sets the previously active simulation as the active selection.
        /// </summary>
        internal void RestorePreviousSelection()
        {
            ActiveSelection = previousSelection;
            if (previousSelection != null)
            {
                currentSelectionHighlighter.ActiveSelection = previousSelection.Selection;
                ShowCurrentSelection(true);
            }
            else
            {
                ShowCurrentSelection(false);
            }
        }

        private void Start()
        {
            Initialise();
        }

        private GameObject FindObjectByName(string name)
        {
            var obj = GameObject.Find(name);
            if (obj == null)
                throw new NullReferenceException(
                    $"{name} object could not be found, and was not set in the inspector.");
            return obj;
        }

        private void Initialise()
        {
            if (selectionEditingRendererRoot == null)
                selectionEditingRendererRoot = FindObjectByName("SelectionEditting");
            if (rootObjectForSelectionRendering == null)
                rootObjectForSelectionRendering = FindObjectByName("ActiveSelectionRenderers");
            if (rootObjectForSelections == null) rootObjectForSelections = FindObjectByName("ActiveSelections");

            currentSelectionHighlighter = FindObjectOfType<TransparentSelectionHighlighter>();
            currentSelectionHighlighter.gameObject.SetActive(false);
            possibleSelectionHighlighter = FindObjectOfType<SelectionHighlighter>();
            initialised = true;

            var renderManagers = FindObjectsOfType<RendererManager>();
            var foundDefaultLayer = BaseLayerRendererManager != null;
            if (foundDefaultLayer == false)
            {
                foreach (var renderManager in renderManagers)
                    if (renderManager.BaseLayer || renderManager.name == "BaseRendering")
                    {
                        BaseLayerRendererManager = renderManager;
                        foundDefaultLayer = true;
                        break;
                    }
            }

            if (foundDefaultLayer == false)
                Debug.LogError(
                    "Failed to find the default base layer. Have you marked the RenderManager as BaseLayer?");

            playbackRenderer = FindObjectOfType<VrPlaybackRenderer>();
            networkManager = FindObjectOfType<NetworkManager>();
            networkManager.ClientCreated += OnConnectionStarted;
            
            //playbackRenderer.NameUpdated += OnSimulationNameReceived;
        }

        private void OnSimulationNameReceived(string bane)
        {
            simulationNameReceived = true;
            simulationName = name;
        }

        /// <summary>
        ///     Deletes all selections except base layer.
        /// </summary>
        public void ClearSelections()
        {
            while (Selections.Count > 0) DeleteSelection(Selections[0]);
        }

        private void OnConnectionStarted(object sender, ClientCreatedEventArgs args)
        {
            newSimulation = true;
            networkManager.CurrentConnection?.SubscribeMessage("/top/selectiondata", OnSelectionDataMessage);
        }

        private void OnSelectionDataMessage(string address, params object[] parameters)
        {
            selectionDataReceived = true;
            selectionData = (string)parameters[0];
        }

        // Update is called once per frame
        private void Update()
        {
            if (newSimulation)
            {
                newSimulation = false;
                ClearSelections();
                //if we received a simulation name, attempt to load previous selections.
                if (simulationNameReceived)
                {
                    simulationNameReceived = false;
                    string path = $"{Application.dataPath}/SimulationSettings/{simulationName}.json";

                    if (File.Exists(path))
                    {
                        LoadSelections(path, true);
                    }
                }
            }
            if (selectionDataReceived)
            {
                selectionDataReceived = false;
                if (selectionData != null) LoadSelections(selectionData, false);
            }
        }
        
        public void SelectBackbone()
        {
            var backboneIDs = GetBackboneAtoms(ActiveSelection.Selection);
            if (backboneIDs != null)
            {
                ActiveSelection.Selection.RemoveAll();
                ActiveSelection.Selection.AddRange(backboneIDs);
            }
        }

        //Method which looks through an NSBSelection and picks out protien backbone atoms
        private HashSet<int> GetBackboneAtoms(NSBSelection selection, int selectionMode = 0)
        {
            var backboneAtoms = new HashSet<int>();
            //Find current atoms in frame
            var frame = this.playbackRenderer.Frame;
            frame.Topology.UpdateVisibleAtomMap(frame.VisibleAtomMap);
            var atoms = frame.Topology.VisibleAtomMap;

            //Check selection is populated
            if (selection != null)
                foreach (var atomIndex in selection)
                {
                    var atom = atoms[atomIndex];
                    var proteinAtom = atom as ProteinAtom;
                    if (proteinAtom != null)
                    {
                        // we're only interested in the backbone atoms
                        var c = atom.Name.Length == 1 && atom.Name[0] == 'C';
                        var n = atom.Name.Length == 1 && atom.Name[0] == 'N';
                        var o = atom.Name.Length == 1 && atom.Name[0] == 'O';
                        var ca = atom.Name.Length == 2 && atom.Name[0] == 'C' && atom.Name[1] == 'A';
                        var h = atom.Name == "H" || atom.Name == "HA";

                        if ((c || n || o || ca) && selectionMode == 0)
                            backboneAtoms.Add(atomIndex);
                        else if (!(c || n || o || ca || h) && selectionMode == 1) backboneAtoms.Add(atomIndex);
                    }
                }

            //Check change has been made
            if (backboneAtoms.Count == 0) return null;
            return backboneAtoms;
        }
    }
}