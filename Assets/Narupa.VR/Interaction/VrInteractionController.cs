﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Transport.Variables.Interaction;
using Narupa.Client.UI.Pointer;
using Narupa.VR.Controller;
using Narupa.VR.Multiplayer;
using Narupa.VR.Player.Control;
using Narupa.VR.UI.Pointer;
using NSB.Processing;
using UnityEngine;
using Valve.VR;

namespace Narupa.VR.Interaction
{
    /// <summary>
    ///     Class for controlling user interactions with a simulation in VR. Handles application of force as well as
    ///     transformations.
    /// </summary>
    internal class VrInteractionController : MonoBehaviour
    {
        /// <summary>
        ///     Whether to run interactions in dance mode. In dance mode, controllers passively attract all nearby atoms.
        /// </summary>
        [Header("Interaction Options")] [SerializeField]
        public static bool DanceMode;

        /// <summary>
        ///     Whether all interactions are disabled.
        /// </summary>
        public static bool InteractionsDisabled = false;

        /// <summary>
        ///     Scale factor that can be modified using VR menus.
        /// </summary>
        public static float GradientScaleFactor = 1f;

        /// <summary>
        ///     Global scale factor that can be modified using in game option menus.
        /// </summary>
        public static float GlobalGradientScaleFactor = 1f;

        /// <summary>
        ///     Whether transforming of the narupa is enabled.
        /// </summary>
        public static bool EnableTransform = true;

        /// <summary>
        ///     Enum that decides which hand's input is used
        /// </summary>
        [SerializeField] private SteamVR_Input_Sources inputSource;
        
        /// <summary>
        ///     SteamVR Input action that acts as a 'trigger' to move objects
        /// </summary>
        [SerializeField] private SteamVR_Action_Boolean actionTrigger;
        
        /// <summary>
        ///     SteamVR Input action that acts as a 'grip' to move objects
        /// </summary>
        [SerializeField] private SteamVR_Action_Boolean actionGrip;
        
        private byte deviceId;

        private bool gripState;

        /// <summary>
        ///     The transform where interactions will be centered.
        /// </summary>
        [SerializeField] private Transform interactionCenter;

        [SerializeField] private GameObject interactionCursorActive;
        [SerializeField] private GameObject interactionCursorInactive;

        /// <summary>
        ///     Interaction type the controllers will apply.
        /// </summary>
        public InteractionType InteractionType =
            InteractionType.Group;

        private LayerInteractionController layerInteractionController;

        private UiPointerInputModule inputModule;
        private VrPlaybackRenderer playback;
        private int translationPointIndex;

        private VrControllerStateManager controllerStateManager;

        private bool triggerState;

        private WorldLocalSpaceTransformer vrRelativeRootTransform;

        private void Awake()

        {
            vrRelativeRootTransform = FindObjectOfType<WorldLocalSpaceTransformer>();
            inputModule = FindObjectOfType<UiPointerInputModule>();

            if (layerInteractionController == null)
                layerInteractionController = FindObjectOfType<LayerInteractionController>();
            playback = FindObjectOfType<VrPlaybackRenderer>();

            controllerStateManager = FindObjectOfType<VrControllerStateManager>();
        }

        private void OnValidate()
        {
            EnableDanceMode(DanceMode);
        }

        private void Start()
        {
            EnableDanceMode(DanceMode);
        }

        private void Update()
        {
            var triggerDownState = actionTrigger.GetState(inputSource);
            var newGripState = actionGrip.GetState(inputSource);

            var interactionPosition = interactionCenter.position;
            var interactionQuaternion = transform.rotation;

            if (EnableTransform)
            {
                if (gripState == false && newGripState)
                    translationPointIndex =
                        vrRelativeRootTransform.RegisterTransformPoint(interactionPosition, interactionQuaternion);
                //else if (GripState == true && touchpadState == false)
                else if (gripState && newGripState == false)
                    vrRelativeRootTransform.ReleaseTransformPoint(translationPointIndex);
                // else if (touchpadState == true)
                else if (newGripState)
                    vrRelativeRootTransform.UpdateTransformPoint(translationPointIndex, interactionPosition,
                        interactionQuaternion);
            }

            gripState = newGripState;
            // Interaction applied either when trigger pulled or in dance mode.
            if (triggerDownState && !inputModule.isPointerActive || DanceMode)
            {
                //TODO this needs tidying.
                if (InteractionsDisabled)
                {
                    //Disabled the haptic feedback on interactions being disabled as it conflicts with selection mode.
                    /*
                    if (controllerEvents)
                    {
                        VRTK_ControllerHaptics.TriggerHapticPulse(
                            VRTK_ControllerReference.GetControllerReference(controllerEvents.gameObject),
                            disabledInteractionFeedbackStrength);
                    }
                    */
                }
                else if (InteractionType == InteractionType.Group)
                {
                    NSBFrame frame = playback.LastFullFrame;
                    
                    if (frame != null)
                    {
                        //If this is the first frame where we've pulled the trigger, add a new interaction to work out what we're hitting.
                        if (triggerState == false)
                        {
                            deviceId = (byte) this.inputSource;
                            layerInteractionController.AddInteraction(interactionCenter.position, frame, deviceId,
                                GradientScaleFactor);
                            SetInteractionCursorActive(true);
                        }
                        //Otherwise, just update the existing interactions.
                        else
                        {
                            layerInteractionController.UpdateInteractionPosition(deviceId, interactionCenter.position);
                        }
                    }
                }
                //Legacy way of performing interactions.
                else
                {
                    deviceId = (byte) this.inputSource;
                    var interaction =
                        new Nano.Transport.Variables.Interaction.Interaction
                        {
                            GradientScaleFactor = GradientScaleFactor * GlobalGradientScaleFactor,
                            InteractionType = (byte) InteractionType,
                            PlayerID = SteamVrObjectSender.PlayerId,
                            InputID = deviceId
                        };
                    layerInteractionController.AddLegacyInteraction(interactionCenter, interaction);
                    SetInteractionCursorActive(true);
                }
            }
            else
            {
                layerInteractionController.DeleteInteraction(deviceId);
                SetInteractionCursorActive(false);
            }

            triggerState = triggerDownState;
        }

        private void SetInteractionCursorActive(bool active)
        {
            interactionCursorActive.SetActive(active);
            interactionCursorInactive.SetActive(!active);
            controllerStateManager.SetCursorActive(this.gameObject, active);
        }

        /// <summary>
        ///     Enables or disables Dance Mode.
        /// </summary>
        /// <param name="enable"></param>
        /// <remarks>
        ///     In Dance Mode, interaction is applied to all nearby atoms, and the in world VR UI is disabled.
        /// </remarks>
        private void EnableDanceMode(bool enable)
        {
            DanceMode = enable;
            if (enable)
                InteractionType = InteractionType.AllAtom;
            else
                InteractionType = InteractionType.Group;
        }
    }
}