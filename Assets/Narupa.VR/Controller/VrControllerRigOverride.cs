﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Linq;
using UnityEngine;

namespace Narupa.VR.Controller
{
	/// <summary>
	/// 	Adds children to SteamVR controllers, allowing the default SteamVR [CameraRig] and the controller
	/// 	prefabs to be separate
	/// </summary>
	public class VrControllerRigOverride : MonoBehaviour
	{
		[SerializeField] private Transform steamVrLeftController;
		[SerializeField] private Transform steamVrRightController;
		[SerializeField] private Transform customLeftController;
		[SerializeField] private Transform customRightController;
		[SerializeField] private bool disableSteamVRModel;

		// Use this for initialization
		void Awake ()
		{
			// Reassign children to the SteamVR controllers
			var leftChildren = Enumerable.Range(0, customLeftController.childCount).Select(i => customLeftController.GetChild(i)).ToArray();
			foreach (var child in leftChildren)
				child.parent = steamVrLeftController;
			var rightChildren = Enumerable.Range(0, customRightController.childCount).Select(i => customRightController.GetChild(i)).ToArray();
			foreach (var child in rightChildren)
				child.parent = steamVrRightController;
			if (disableSteamVRModel)
			{
				steamVrLeftController.Find("Model")?.gameObject.SetActive(false);
				steamVrRightController.Find("Model")?.gameObject.SetActive(false);
			}
			
		}

	}
}
