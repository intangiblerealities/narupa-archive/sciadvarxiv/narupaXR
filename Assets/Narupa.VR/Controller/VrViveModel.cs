﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;
using Valve.VR;

namespace Narupa.VR.Controller
{
	/// <summary>
	/// 	Component which manages the Vive controller model. This handles glowing triggers and grips depending
	/// 	on the context, as well as lighting the menu button of the left controller in orange.
	/// </summary>
	public class VrViveModel : MonoBehaviour
	{
		[Header("Materials")]

		[Tooltip("Base material for the body of the controller")]
		[SerializeField] protected Material MaterialBase;
	
		[Tooltip("Material used for general buttons (purple)")]
		[SerializeField] protected Material MaterialButton;
	
		[Tooltip("Material used for important buttons (orange)")]
		[SerializeField] protected Material MaterialButton2;

		[Tooltip("Material used for the end colored by mode")]
		[SerializeField] protected Material MaterialMode;
		
		[Header("Controller Components")]
		[SerializeField] protected GameObject Trigger;
		[SerializeField] protected GameObject MenuButton;
		[SerializeField] protected GameObject Grip;

		[Header("Controller State")]
		[Tooltip("Should the menu button on this controller glow orange")]
		[SerializeField] protected bool MenuGlow = false;

		[Header("SteamVR Input")]
		[SerializeField] protected SteamVR_Action_Boolean ActionTrigger;
		[SerializeField] protected SteamVR_Action_Boolean ActionGrip;
		[SerializeField] protected SteamVR_Input_Sources InputSource;

		protected void Awake()
		{
			var materialBaseOld = this.MaterialBase;
			var materialButtonOld = this.MaterialButton;
			var materialButton2Old = this.MaterialButton2;
			var materialModeOld = this.MaterialMode;
			// Change all the child renderers to use instanced materials
			this.MaterialBase = Object.Instantiate(this.MaterialBase);
			this.MaterialButton = Object.Instantiate(this.MaterialButton);
			this.MaterialButton2 = Object.Instantiate(this.MaterialButton2);
			this.MaterialMode = Object.Instantiate(this.MaterialMode);
			foreach (var renderer in this.GetComponentsInChildren<Renderer>())
			{
				if (renderer.sharedMaterial == materialBaseOld)
					renderer.sharedMaterial = this.MaterialBase;
				if (renderer.sharedMaterial == materialButtonOld)
					renderer.sharedMaterial = this.MaterialButton;
				if (renderer.sharedMaterial == materialButton2Old)
					renderer.sharedMaterial = this.MaterialButton2;
				if (renderer.sharedMaterial == materialModeOld)
					renderer.sharedMaterial = this.MaterialMode;
			}
		}
		
		// Use this for initialization
		protected void Start ()
		{
			// enable/disable the menu button
			SetPartActive(MenuButton, MenuGlow);
		}

		/// <summary>
		/// 	Sets a part of the controller to be active or passive (orange or purple)
		/// </summary>
		/// <param name="obj">The component to alter</param>
		/// <param name="active">Whether it is active (orange) or passive (purple)</param>
		protected void SetPartActive(GameObject obj, bool active)
		{
			foreach(var renderer in obj.GetComponentsInChildren<Renderer>())
				renderer.material = active ? MaterialButton2 : MaterialButton;
		}
	
		// Update is called once per frame
		protected void Update () {
			// Update the trigger and grip buttons with the current state of the appropriate action
			SetPartActive(Trigger, ActionTrigger.GetState(InputSource));
			SetPartActive(Grip, ActionGrip.GetState(InputSource));
		}

		public void UpdateColors(Color primary, Color secondary)
		{
			if (Application.isPlaying)
			{
				this.MaterialBase.SetColor("_OutlineColor", primary);
				this.MaterialButton.color = primary;
				this.MaterialButton2.color = secondary;
				this.MaterialMode.color = primary;
			}
		}
		
		public void UpdateBodyColors(Color primary, Color secondary)
		{
			if (Application.isPlaying)
			{
				this.MaterialBase.SetColor("_OutlineColor", primary);
				this.MaterialBase.SetColor("_Color", secondary);
				this.MaterialMode.color = primary;
			}
		}

	}
}
