﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;

namespace Narupa.VR.Controller
{
	/// <summary>
	/// 	Object stored in Assets/ which represents a possible mode for the controllers. These are added to the
	/// 	app by adding them to the Modes variable of the VrControllerModeManager
	/// </summary>
	[CreateAssetMenu(fileName = "Mode", menuName = "Controller Mode")]
	public class ControllerMode : ScriptableObject
	{
		/// <summary>
		/// 	Color used for the outline of the controller in this mode
		/// </summary>
		public Color PrimaryColor;
		
		/// <summary>
		/// 	Color used for the body of the controller in this mode
		/// </summary>
		public Color PrimaryColorBody;
	}
}
