﻿using System.Linq;
using Narupa.Utility.Unity;
using NUnit.Framework;
using UnityEngine;

namespace Narupa.Utility.Tests
{
    public class MatrixExtensionsTests
    {
        private static Matrix4x4 GetRandomTRSMatrix(Vector3? translation = null,
                                                    Quaternion? rotation = null,
                                                    Vector3? scale = null)
        {
            return Matrix4x4.TRS(translation ?? Random.onUnitSphere, 
                                 rotation ?? Random.rotation, 
                                 scale ?? Vector3.one * Random.value);
        }

        private const float tolerance = 0.001f;

        [Test, Pairwise]
        public void GetTranslation_OfRandomTRS_ReturnsInputTranslation(
            [ValueSource(typeof(TransformTestData), nameof(TransformTestData.TrackingPoints))] Vector3 translation,
            [ValueSource(typeof(TransformTestData), nameof(TransformTestData.Rotations))] Quaternion rotation,
            [ValueSource(typeof(TransformTestData), nameof(TransformTestData.UniformScales))] float scale)
        {
            var matrix = Matrix4x4.TRS(translation, rotation, Vector3.one * scale);
                
            Assert.AreEqual(translation, matrix.GetTranslation());
        }

        [Test, Pairwise]
        public void GetRotation_OfRandomTRS_ReturnsInputRotation(
            [ValueSource(typeof(TransformTestData), nameof(TransformTestData.TrackingPoints))] Vector3 translation,
            [ValueSource(typeof(TransformTestData), nameof(TransformTestData.Rotations))] Quaternion rotation,
            [ValueSource(typeof(TransformTestData), nameof(TransformTestData.UniformScales))] float scale)
        {
            var matrix = Matrix4x4.TRS(translation, rotation, Vector3.one * scale);

            var extractedRotation = matrix.GetRotation();
            float difference = Mathf.Abs(Quaternion.Dot(rotation, extractedRotation));

            Assert.GreaterOrEqual(difference, 1 - tolerance);
        }

        [Test, Pairwise]
        public void GetScale_WithUniformScaleTRS_ReturnsInputScale(    
            [ValueSource(typeof(TransformTestData), nameof(TransformTestData.TrackingPoints))] Vector3 translation,
            [ValueSource(typeof(TransformTestData), nameof(TransformTestData.Rotations))] Quaternion rotation,
            [ValueSource(typeof(TransformTestData), nameof(TransformTestData.UniformScales))] float scale)
        {
            var matrix = Matrix4x4.TRS(translation, rotation, Vector3.one * scale);
            
            TestAsserts.AreApproximatelyEqual(Vector3.one * scale, matrix.GetScale());
        }

        [Test]
        public void AllTRSGetters_WithUniformScaleTRS_ReturnInputProperties(    
            [ValueSource(typeof(TransformTestData), nameof(TransformTestData.TrackingPoints))] Vector3 translation,
            [ValueSource(typeof(TransformTestData), nameof(TransformTestData.Rotations))] Quaternion rotation,
            [ValueSource(typeof(TransformTestData), nameof(TransformTestData.UniformScales))] float scale)
        {
            var matrix = Matrix4x4.TRS(translation, rotation, Vector3.one * scale);

            var extractedTranslation = matrix.GetTranslation();
            var extractedRotation = matrix.GetRotation();
            var extractedScale = matrix.GetScale();

            float difference = Mathf.Abs(Quaternion.Dot(rotation, extractedRotation));

            float scaleError = Vector3.Distance(Vector3.one * scale, extractedScale);

            Assert.AreEqual(translation, extractedTranslation);
            Assert.GreaterOrEqual(difference, 1 - tolerance);
            Assert.LessOrEqual(scaleError, tolerance);
        }
    }
}
