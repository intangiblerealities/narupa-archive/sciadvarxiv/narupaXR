﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using UnityEngine;

namespace Narupa.Utility.Unity
{
    public static class UnityUtility
    {
        private static readonly Dictionary<PrimitiveType, UnityEngine.Mesh> primitiveMeshes =
            new Dictionary<PrimitiveType, UnityEngine.Mesh>();

        public static T InstantiateWithoutRename<T>(T obj) where T : Object
        {
            var created = Object.Instantiate(obj);
            created.name = obj.name;
            return created;
        }

        public static T InstantiateWithoutRename<T>(T obj, Transform parent) where T : Object
        {
            var created = Object.Instantiate(obj, parent);
            created.name = obj.name;
            return created;
        }

        public static T InstantiateWithoutRename<T>(this Object bse, T obj) where T : Object
        {
            return InstantiateWithoutRename(obj);
        }

        public static T InstantiateWithoutRename<T>(this Object bse, T obj, Transform parent) where T : Object
        {
            return InstantiateWithoutRename(obj, parent);
        }

        /// <summary>
        ///     Get a primitive Unity mesh (sphere, cube, etc.)
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static UnityEngine.Mesh GetPrimitiveMesh(PrimitiveType type)
        {
            if (primitiveMeshes.ContainsKey(type))
                return primitiveMeshes[type];
            var obj = GameObject.CreatePrimitive(type);
            primitiveMeshes[type] = obj.GetComponent<MeshFilter>().sharedMesh;
            Object.Destroy(obj);
            return primitiveMeshes[type];
        }
    }
}