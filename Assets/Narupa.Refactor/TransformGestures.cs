﻿// Copyright (c) 2019 Alex Jamieson-Binnie & Mark Wonnacott. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.Utility.Unity;
using UnityEngine;

/// <summary>
/// Gesture that ties an object's position and orientation to a control point's 
/// position and orientation. Updating the control point matrix yields an 
/// updated object matrix such that the object has moved to preserve the
/// relative position and orientation of the object and control point.
/// 
/// e.g a single hand grabbing a box, the box then moves and rotates with the
/// the motion of the hand.
/// </summary>
public class OnePointTranslateRotateGesture
{
    private Matrix4x4 controlPointToObjectMatrix;

    /// <summary>
    /// Begin the gesture with an initial object transform and initial control
    /// point transform. Throughout the gesture the relationship between these
    /// transforms will remain invariant.
    /// </summary>
    public void BeginGesture(Matrix4x4 objectMatrix,
                             Matrix4x4 controlPointMatrix)
    {
        controlPointToObjectMatrix = MatrixExtensions.GetRelativeTransformMatrix(from: controlPointMatrix, 
                                                                                 to: objectMatrix);
    }

    /// <summary>
    /// Return an object matrix that is the initial object matrix transformed
    /// in the same manner as the gesture transform since the gesture began.
    /// 
    /// The relative coordinates of the gesture within the object space 
    /// remains invariant.
    /// </summary>
    public Matrix4x4 UpdateControlPoint(Matrix4x4 gestureMatrix)
    {
        var objectMatrix = gestureMatrix * controlPointToObjectMatrix;

        return objectMatrix;
    }
}

/// <summary>
/// Gesture that ties an object's position, orientation, and scale to the
/// positions of two control points. Updating the control point matrices yields
/// an updated object matrix such that the object has moved to preserve the
/// local positions of the control points within the object. 
/// 
/// e.g two hands grab corners of a box, and as the hands move the box 
/// stretches and pivots with the motion of the hands.
/// </summary>
public class TwoPointTranslateRotateScaleGesture
{
    private OnePointTranslateRotateGesture onePointGesture = new OnePointTranslateRotateGesture();

    /// <summary>
    /// Begin the gesture with an initial object transform and intial 
    /// transforms for the two control points. Throughout the gesture the 
    /// positions of the control points within object space will remain 
    /// invariant.
    /// </summary>
    public void BeginGesture(Matrix4x4 objectMatrix,
                             Matrix4x4 controlPointMatrix1,
                             Matrix4x4 controlPointMatrix2)
    {
        var midpointMatrix = ComputeMidpointMatrix(controlPointMatrix1, controlPointMatrix2);
        onePointGesture.BeginGesture(objectMatrix, midpointMatrix);
    }

    /// <summary>
    /// Return an updated object matrix to account for the changes in the
    /// control point transforms.
    /// </summary>
    public Matrix4x4 UpdateControlPoints(Matrix4x4 controlPointMatrix1,
                                         Matrix4x4 controlPointMatrix2)
    {
        var midpointMatrix = ComputeMidpointMatrix(controlPointMatrix1, controlPointMatrix2);
        var objectMatrix = onePointGesture.UpdateControlPoint(midpointMatrix);

        return objectMatrix;
    }

    /// <summary>
    /// Compute a TRS matrix that reprents the "average" of two input TRS
    /// matrices. 
    /// </summary>
    private static Matrix4x4 ComputeMidpointMatrix(Matrix4x4 controlPointMatrix1, 
                                                   Matrix4x4 controlPointMatrix2)
    {
        // position the origin between the two control points
        var position1 = controlPointMatrix1.GetTranslation();
        var position2 = controlPointMatrix2.GetTranslation();
        var position = Vector3.LerpUnclamped(position1, position2, 0.5f);

        // base the scale on the seperation between the two control points
        var scale = Vector3.Distance(position1, position2);

        // choose an orientation where the line between the two control points
        // is one axis, and another axis is roughly the compromise between the
        // two up vectors of the control points
        var rotation1 = controlPointMatrix1.GetRotation();
        var rotation2 = controlPointMatrix2.GetRotation();
        var midRotation = Quaternion.SlerpUnclamped(rotation1, rotation2, 0.5f);

        var up = midRotation * Vector3.up;
        var right = (position2 - position1).normalized;

        var rotation = Quaternion.LookRotation(right, up);
            
        return Matrix4x4.TRS(position, rotation, scale * Vector3.one);
    }
}
