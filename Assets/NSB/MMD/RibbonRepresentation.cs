﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using System.Threading.Tasks;
using NSB.MMD.Base;
using NSB.MMD.MeshGeneration;
using NSB.MMD.RenderingTypes;
using NSB.MMD.Splines;
using NSB.Processing;
using NSB.Utility;
using UnityEngine;
using UnityEngine.Profiling;

namespace NSB.MMD
{
    public class RibbonRepresentation : MMDRenderer, ISelectionSource
    {
        [System.Serializable]
        public class RibbonConfig : RendererConfig
        {
            public FloatParameter Width;
            public IntParameter IntermediatePoints;
            public FloatParameter SplineAlpha;
            public BoolParameter Untwist;

            public BoolParameter HelenSpline;
            public BoolParameter HelenSmoothing;

            protected override void Setup()
            {
                Width = AddFloat(nameof(Width), 0.04f, 0, 0.1f);
                IntermediatePoints = AddInt(nameof(IntermediatePoints), 0, 0, 8);
                SplineAlpha = AddFloat(nameof(SplineAlpha), 0.5f, 0, 1);
                HelenSmoothing = AddBool(nameof(HelenSmoothing), false);
                Untwist = AddBool(nameof(Untwist), false);
                HelenSpline = AddBool(nameof(HelenSpline), false);
            }
        }

        private MeshFilter ribbonMeshFilter;
        private MeshRenderer ribbonMeshRenderer;

        private MeshTool mesh;

        private HashSet<ushort> isControl = new HashSet<ushort>();
        private HashSet<ushort> isNormal = new HashSet<ushort>();

        private List<ushort> path = new List<ushort>();
        private List<Vector3> controlPoints = new List<Vector3>();
        private List<Color32> controlColors = new List<Color32>();
        private List<ushort> controlAtoms = new List<ushort>();
        private List<Vector3> controlNormals = new List<Vector3>();
        private List<ushort> oxyAtoms = new List<ushort>();

        [Header("Setup")]
        [SerializeField]
        private Material ribbonMaterial;

        public bool LazyUpdates;

        public RibbonConfig Settings = new RibbonConfig();
        public override RendererConfig Config => Settings;

        NSBSelection ISelectionSource.Selection
        {
            get
            {
                return backbone;
            }
        }

        private void Awake()
        {
            AddMesh(ref ribbonMeshFilter, ref ribbonMeshRenderer);

            mesh = new MeshTool(new Mesh(), MeshTopology.LineStrip);
            ribbonMeshFilter.sharedMesh = mesh.mesh;
            ribbonMeshRenderer.sharedMaterial = new Material(ribbonMaterial);
        }

        private NSBSelection backbone = new NSBSelection();

        private void SelectBackbone(NSBSelection selection)
        {
            // start from scratch
            isControl.Clear();
            selection.Clear();
            selection.SetSourceAtomCount(Frame.AtomCount);

            // iterate over every visible atom to work out which ones are relevant
            // TODO: i guess we can select by topology information...
            Frame.Topology.UpdateVisibleAtomMap(Frame.VisibleAtomMap);
            var atoms = Frame.Topology.VisibleAtomMap;

            for (int i = 0; i < Selection.Count; ++i)
            {
                int id = Selection[i];
                var atom = atoms[id];

                // we're only interested in the backbone atoms
                bool C = atom.Name.Length == 1 && atom.Name[0] == 'C';
                bool N = atom.Name.Length == 1 && atom.Name[0] == 'N';
                bool O = atom.Name.Length == 1 && atom.Name[0] == 'O';
                bool CA = atom.Name.Length == 2 && atom.Name[0] == 'C' && atom.Name[1] == 'A';

                if (C || N || O || CA)
                {
                    selection.Add(id);

                    // keep a fast lookup for whether atoms are considered the control atoms
                    if (CA) isControl.Add((ushort)id);
                    if (O) isNormal.Add((ushort)id);
                }
            }
        }

        private ushort[] normalAtom;
        private List<byte> bondCounts = new List<byte>();
        private List<ushort> successors = new List<ushort>();

        /// <summary>
        /// Trace a linked path through the backbone atoms so we have them in order
        /// </summary>
        private void TracePath(NSBFrame frame,
            NSBSelection selection,
            List<ushort> path)
        {
            bondCounts.Resize(frame.AtomCount);
            successors.Resize(frame.AtomCount * 2);

            if (normalAtom == null || normalAtom.Length < frame.AtomCount)
            {
                normalAtom = new ushort[frame.AtomCount];
            }
            else
            {
                for (int i = 0; i < frame.AtomCount; ++i)
                {
                    bondCounts[i] = 0;
                    successors[i * 2 + 0] = 0;
                    successors[i * 2 + 1] = 0;
                }
            }

            for (int i = 0; i < frame.BondCount; ++i)
            {
                var bond = frame.BondPairs[i];

                if (selection.Contains(bond))
                {
                    if (isNormal.Contains(bond.A) && selection.Contains(bond.B))
                    {
                        normalAtom[bond.B] = bond.A;
                    }
                    else if (isNormal.Contains(bond.B) && selection.Contains(bond.A))
                    {
                        normalAtom[bond.A] = bond.B;
                    }
                    else
                    {
                        successors[bond.A * 2 + bondCounts[bond.A]] = bond.B;
                        successors[bond.B * 2 + bondCounts[bond.B]] = bond.A;

                        bondCounts[bond.A] += 1;
                        bondCounts[bond.B] += 1;
                    }
                }
            }

            int start = selection[0];

            for (int i = 0; i < frame.AtomCount; ++i)
            {
                if (bondCounts[i] == 1)
                {
                    start = i;
                    break;
                }
            }

            path.Clear();
            ushort id = (ushort)start;
            path.Add(id);

            // no acyclic path can be longer than the selection, so use it as an
            // emergency termination condition
            int limit = selection.Count;

            do
            {
                int prev = path.Count > 1 ? path[path.Count - 2] : path[path.Count - 1];
                ushort a = successors[id * 2 + 0];
                ushort b = successors[id * 2 + 1];

                id = (a == prev) ? b : a;

                path.Add(id);
            }
            while (bondCounts[id] > 1 && limit >= 0);

            controlAtoms.Clear();
            oxyAtoms.Clear();
            for (int i = 0; i < path.Count; ++i)
            {
                id = path[i];

                if (isControl.Contains(id))
                {
                    controlAtoms.Add(id);
                }
                else if (normalAtom[id] > 0)
                {
                    oxyAtoms.Add(id);
                }
            }
        }

        private void RefreshTopology(NSBFrame frame)
        {
            if (frame.Topology == null) return;

            Profiler.BeginSample("RibbonLineMeshRenderer.RefreshTopology");

            SelectBackbone(backbone);
            TracePath(frame, backbone, path);

            Profiler.EndSample();
        }

        private void Resize(int vertCount)
        {
            if (mesh.VertexCount != vertCount)
            {
                mesh.mesh.Clear();
                mesh.SetVertexCount(vertCount);
            }

            if (mesh.indices == null || mesh.indices.Length != vertCount)
            {
                mesh.SetIndexCount(vertCount, lazy: false);

                // just colour based on index...
                Parallel.For(0, vertCount, i =>
                {
                    mesh.indices[i] = i;
                });

                mesh.Apply(positions: true, colors: true, indices: true);
            }
        }

        public override void Refresh()
        {
            if (!CheckValidity() || Selection.Count == 0) return;

            if (!LazyUpdates)
            {
                RefreshTopology(Frame);
            }

            if (controlAtoms.Count < 2)
            {
                mesh.mesh.Clear();
                return;
            }

            ribbonMeshRenderer.sharedMaterial.SetFloat("_Scaling", Settings.Width.Value);

            if (Settings.HelenSpline.Value)
            {
                controlPoints.Resize(path.Count);
                Resize(HelenBezierSpline.GetPointCount(path.Count, Settings.IntermediatePoints.Value));
                Parallel.For(0, path.Count, i => controlPoints[i] = Frame.AtomPositions[path[i]]);
                HelenBezierSpline.ComputeLine(controlPoints, mesh.positions, mesh.normals, Settings.IntermediatePoints.Value);
            }
            else
            {
                controlPoints.Resize(controlAtoms.Count);
                controlColors.Resize(controlAtoms.Count);
                controlNormals.Resize(controlAtoms.Count);
                Resize(CatmulRomSpline.GetPointCount(controlPoints.Count, Settings.IntermediatePoints.Value));

                // copy atom positions into control points list
                Parallel.For(0, controlAtoms.Count, i =>
                {
                    controlPoints[i] = Frame.AtomPositions[controlAtoms[i]];
                    controlColors[i] = Style.AtomColors[controlAtoms[i]];
                });

                // generate the interpolated points into the vertices list
                CatmulRomSpline.ComputeLine(controlPoints, mesh.positions, Settings.IntermediatePoints.Value, Settings.SplineAlpha.Value);

                // compute the control normals based on two adjacent control points
                // and the corresponding oxygen atom
                Parallel.For(1, oxyAtoms.Count, i =>
                {
                    Vector3 prev = controlPoints[i - 1];
                    Vector3 next = controlPoints[i - 0];
                    Vector3 oxy = Frame.AtomPositions[oxyAtoms[i - 1]];

                    controlNormals[i] = Vector3.Cross(next - prev, oxy - next).normalized;
                });

                // we can't do this for the first one, so just repeat the second
                controlNormals[0] = controlNormals[1];

                // if any normal is further than right angles to the previous then
                // we can flip it to reduce the twisting in the ribbon
                // TODO: seems unstable
                if (Settings.Untwist.Value)
                {
                    for (int i = 1; i < controlNormals.Count; ++i)
                    {
                        Vector3 prev = controlNormals[i - 1];
                        Vector3 curr = controlNormals[i - 0];

                        if (Vector3.Dot(prev, curr) < 0)
                        {
                            controlNormals[i] = -controlNormals[i];
                        }
                    }
                }

                int stride = Settings.IntermediatePoints.Value + 1;
                float inv = 1f / stride;

                Parallel.For(1, controlNormals.Count, i =>
                {
                    Color32 p0 = controlColors[i - 1];
                    Color32 p1 = controlColors[i + 0];

                    for (int v = 0; v < stride; ++v)
                    {
                        mesh.colors[(i - 1) * stride + v] = Color32.LerpUnclamped(p0, p1, inv * v);
                    }
                });

                mesh.colors[mesh.colors.Count - 1] = controlColors[controlColors.Count - 1];

                // spherically interpolate normals between control normals
                Parallel.For(1, controlNormals.Count, i =>
                {
                    Vector3 p0 = controlNormals[i - 1];
                    Vector3 p1 = controlNormals[i + 0];

                    for (int v = 0; v < stride; ++v)
                    {
                        mesh.normals[(i - 1) * stride + v] = Vector3.Slerp(p0, p1, inv * v);
                    }
                });

                // we miss the last control normal so fill that in
                mesh.normals[mesh.normals.Count - 1] = controlNormals[controlNormals.Count - 1];
            }

            if (Settings.HelenSmoothing.Value) HelenBezierSpline.Smooth(mesh.positions, mesh.normals, 3, 6);

            mesh.Apply(positions: true, normals: true, colors: true);
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.magenta;

            for (int i = 0; i < controlNormals.Count; ++i)
            {
                Gizmos.DrawLine(controlPoints[i], controlPoints[i] + controlNormals[i] * 0.1f);
            }
        }

        public override float GetAtomBoundingRadius(int id)
        {
            //this is unhelpful, as selections need to know what they are highlighting!
            //if (!backbone.Contains(id)) return -1;

            return Settings.Width.Value * 0.5f;
        }
    }
}