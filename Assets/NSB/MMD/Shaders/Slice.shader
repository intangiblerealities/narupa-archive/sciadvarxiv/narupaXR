﻿Shader "NSB/Slice"
{
    Properties
    {
		_MainTex ("Texture", 2D) = "pink" {}
    }

    SubShader
    {
        Tags
        {
            "Queue" = "Geometry"
            "IgnoreProjector" = "True"
            "RenderType" = "Opaque"
            "PreviewType" = "Cube"
        }

        Cull Off
        Lighting Off
        ZWrite On
		Blend One Zero

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION; 
				float2 texcoord : TEXCOORD0;
				float4 color : COLOR;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                float4 color : COLOR;
				float2 uv : TEXCOORD0;
            };

			sampler2D _MainTex;

            v2f vert(appdata v)
            {
                v2f o;

                o.pos =  UnityObjectToClipPos(v.vertex);
                o.color = v.color;
				o.uv = v.texcoord;

                return o;
            }

            half4 frag(v2f i) : COLOR
            {
				half4 tex = tex2D(_MainTex, i.uv);

				return tex.a * i.color;
            }
            ENDCG
        }
    }
}
