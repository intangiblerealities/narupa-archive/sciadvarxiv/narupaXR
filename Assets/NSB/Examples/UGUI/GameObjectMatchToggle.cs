﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;
using UnityEngine.UI;

namespace NSB.Examples.UGUI
{
    public class GameObjectMatchToggle : MonoBehaviour
    {
        [SerializeField]
        private GameObject match;
        [SerializeField]
        private Toggle toggle;
        [SerializeField]
        private bool invert;
    
        private void Awake()
        {
            match.SetActive(toggle.isOn);

            toggle.onValueChanged.AddListener(active =>
            {
                match.SetActive(invert ? !active : active);
            });
        }
    }
}
