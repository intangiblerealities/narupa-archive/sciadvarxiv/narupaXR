﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;

namespace NSB.Examples.Data
{
    /// <summary>
    /// Setup for all the custom serialization for LitJSON and expose two simple 
    /// methods for converting between objects and JSON without explicitly
    /// invoking LitJSON.
    /// </summary>
    public static class JSON 
    {
        static JSON()
        {
            LitJson.UnityTypeBindings.Register();

            LitJson.JsonMapper.RegisterImporter<string, DateTime>(StringToDate);
            LitJson.JsonMapper.RegisterExporter<DateTime>(DateToString);
            LitJson.JsonMapper.RegisterImporter<string, Guid>(StringToGuid);
            LitJson.JsonMapper.RegisterExporter<Guid>(GuidToString);
            LitJson.JsonMapper.RegisterImporter<string, Uri>(StringToURI);
            LitJson.JsonMapper.RegisterExporter<Uri>(URIToString);
            LitJson.JsonMapper.RegisterExporter<float>((obj, writer) => writer.Write(Convert.ToDouble(obj)));
            LitJson.JsonMapper.RegisterImporter<double, float>(input => Convert.ToSingle(input));
        }

        public static string Serialize<T>(T value)
        {
            return LitJson.JsonMapper.ToJson(value);
        }

        public static T Deserialize<T>(string json)
        {
            return LitJson.JsonMapper.ToObject<T>(json);
        }

        public static T Copy<T>(T value)
        {
            return Deserialize<T>(Serialize(value));
        }

        public static DateTime StringToDate(string value)
        {
            return DateTime.Parse(value);
        }

        public static void DateToString(DateTime value, LitJson.JsonWriter writer)
        {
            writer.Write(value.ToString("o"));
        }

        public static Guid StringToGuid(string value)
        {
            return new Guid(value);
        }

        public static void GuidToString(Guid value, LitJson.JsonWriter writer)
        {
            writer.Write(value.ToString());
        }

        public static Uri StringToURI(string value)
        {
            try
            {
                return new Uri(value, UriKind.Relative);
            }
            catch (UriFormatException)
            {
                return new Uri(value, UriKind.RelativeOrAbsolute);
            }
        }

        public static void URIToString(Uri value, LitJson.JsonWriter writer)
        {
            writer.Write(value.ToString());
        }
    }
}
