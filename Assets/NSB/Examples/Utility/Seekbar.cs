﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace NSB.Examples.Utility
{
    public class Seekbar : MonoBehaviour, 
        IPointerDownHandler,
        IPointerUpHandler
    {
        public Slider Slider;

        public bool Dragging { get; private set; }

        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            Dragging = true;
        }

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            Dragging = false;
        }
    }
}
