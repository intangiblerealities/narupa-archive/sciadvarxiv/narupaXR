﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace NSB.Examples.Utility
{
    public class RepeatButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField]
        private Button button;

        public float delay = .5f;

        private float timer;
        private bool held;
        private PointerEventData click;

        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            timer = 0;
            held = true;
            click = eventData;
        }

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            held = false;
        }

        private void OnEnabled()
        {
            held = false;
        }

        private void Update()
        {
            if (held)
            {
                timer += Time.deltaTime;

                while (timer > delay)
                {
                    timer -= delay;

                    button.OnPointerClick(click);
                }
            }
        }
    }
}
