﻿namespace NSB.Examples.Utility.Tweening
{
    class Quartic : IEaser
    {
        public float EaseIn(float ratio)
        {
            return ratio * ratio * ratio * ratio;
        }

        public float EaseOut(float ratio)
        {
            return 1f - (ratio -= 1f) * ratio * ratio * ratio;
        }

        public float EaseInOut(float ratio)
        {
            return (ratio < 0.5f) ? 8f * ratio * ratio * ratio * ratio : -8f * (ratio -= 1f) * ratio * ratio * ratio + 1f;
        }
    }
}
