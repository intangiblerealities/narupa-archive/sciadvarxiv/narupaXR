﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using Nano.Client;
using NSB.Examples.Player.Screens;
using NSB.Examples.Player.Screens.Replay_Select;
using NSB.Examples.Player.Screens.Server_Select;
using NSB.Examples.Utility;
using UnityEngine;
using UnityEngine.EventSystems;

namespace NSB.Examples.Player.Control
{
    public class ExampleAppController : MonoBehaviour
    {
        public PlaybackRenderer playback;

        [SerializeField]
        private GameObject debugLog;
        [SerializeField]
        private ErrorPopup errorPopup;

        public bool ConsoleReporterOutput = false;

        [Header("Screens")]
        [SerializeField]
        private ServerSelectionPanel serverSelect;
        [SerializeField]
        private ReplaySelectionPanel replaySelect;
        [SerializeField]
        private SimulationHUDPanel replayHUD;

        private void Start()
        {
            playback.OnReady += () => playback.VisibleAtoms.Selection.SetAll(true);
        }

        private DateTime lastCloudUpdate;

        public void SetNoPlayback()
        {
            playback.Clear();

            playback.gameObject.SetActive(false);
        }

        public void SetSinglePlayback()
        {
            playback.gameObject.SetActive(true);

            playback.SetFull();
        }

        public void ErrorMessage(string message, string cancel = "Ignore")
        {
            errorPopup.Show($"Something went wrong.\n\n{message}",
                new ButtonEntry(cancel,       errorPopup.Hide),
                new ButtonEntry("See Log",    OpenDebugLog));
        }

        public void NetworkError(string message, 
            Action retry,
            Action ignore = null)
        {
            errorPopup.Show($"Couldn't connect to server.\n\n{message}",
                new ButtonEntry("Ignore", () => { ignore?.Invoke(); errorPopup.Hide(); }),
                new ButtonEntry("Retry", () => { retry(); errorPopup.Hide(); }));
        }

        public void OpenDebugLog()
        {
            debugLog.SetActive(true);
        }

        private void HideAll()
        {
            replayHUD.gameObject.SetActive(false);
            serverSelect.gameObject.SetActive(false);
            replaySelect.gameObject.SetActive(false);
        }

        public void EnterServerSelection()
        {
            SetNoPlayback();
            HideAll();
            serverSelect.gameObject.SetActive(true);
        }

        public void EnterReplaySelection()
        {
            SetNoPlayback();
            HideAll();
            replaySelect.gameObject.SetActive(true);
        }

        public void EnterServer(SimboxConnectionInfo info)
        {
            HideAll();
            replayHUD.gameObject.SetActive(true);
            SetSinglePlayback();
            playback.EnterServer(info);
        }

        public void EnterRecording(TextAsset asset)
        {
            HideAll();
            replayHUD.gameObject.SetActive(true);
            SetSinglePlayback();
            playback.EnterRecording(asset.bytes);
        }

        private static PointerEventData pointer;
        private static List<RaycastResult> hits = new List<RaycastResult>();
        public static bool IsPointBlocked(Vector2 point)
        {
            pointer = pointer ?? new PointerEventData(EventSystem.current);
            pointer.position = point;

            hits.Clear();
            EventSystem.current.RaycastAll(pointer, hits);

            return hits.Count > 0;
        }

    }
}
