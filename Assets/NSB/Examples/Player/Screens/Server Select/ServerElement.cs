﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Client;
using NSB.Utility;
using UnityEngine;
using UnityEngine.UI;
using Text = TMPro.TextMeshProUGUI;

namespace NSB.Examples.Player.Screens.Server_Select
{
    public class ServerElement : InstanceView<SimboxConnectionInfo>
    {
        [SerializeField]
        private ServerSelectionPanel panel;
        [SerializeField]
        private Button selectButton;
        [SerializeField]
        private Text descriptionText;
        [SerializeField]
        private Text addressText;

        private void Awake()
        {
            selectButton.onClick.AddListener(() => panel.SelectServer(config));
        }

        override protected void Configure()
        {
            descriptionText.text = config.Descriptor;
            addressText.text = string.Format("{0}:{1}", config.Address, config.Port);
        }
    }
}
