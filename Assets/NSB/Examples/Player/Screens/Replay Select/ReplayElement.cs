﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.Utility;
using UnityEngine;
using UnityEngine.UI;
using Text = TMPro.TextMeshProUGUI;

namespace NSB.Examples.Player.Screens.Replay_Select
{
    public class ReplayElement : InstanceView<ReplayInfo>
    {
        [SerializeField]
        private ReplaySelectionPanel panel;
        [SerializeField]
        private Button selectButton;
        [SerializeField]
        private Text descriptionText;

        private void Awake()
        {
            selectButton.onClick.AddListener(() => panel.SelectReplay(config));
        }

        override protected void Configure()
        {
            descriptionText.text = config.asset.name;
        }
    }
}
