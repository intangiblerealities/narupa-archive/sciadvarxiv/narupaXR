﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEditor;
using UnityEngine;
#if UNITY_EDITOR

#endif

namespace NSB.Utility
{
    [RequireComponent(typeof(Camera))]
    public class SaveScreenshot : MonoBehaviour
    {
        public int width, height;

        [ContextMenu("Save Screenshot")]
        public void TakeScreenshot()
        {
#if UNITY_EDITOR
            var camera = GetComponent<Camera>();
            var shotTex = RenderTexture.GetTemporary(width, height, 24);

            {
                var prevTex = camera.targetTexture;
                camera.targetTexture = shotTex;
                camera.Render();
                camera.targetTexture = prevTex;
            }

            RenderTexture.active = shotTex;
            var texture = new Texture2D(shotTex.width, shotTex.height, TextureFormat.ARGB32, false);
            texture.ReadPixels(new Rect(0, 0, texture.width, texture.height), 0, 0);
            RenderTexture.active = null;
            System.IO.File.WriteAllBytes("../screenshot.png", texture.EncodeToPNG());
            AssetDatabase.Refresh();
            Destroy(texture);

            RenderTexture.ReleaseTemporary(shotTex);
#endif
        }
    }
}
