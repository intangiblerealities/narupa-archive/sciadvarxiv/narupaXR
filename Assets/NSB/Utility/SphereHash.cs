﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using NSB.Examples.Utility;
using UnityEngine;

namespace NSB.Utility
{
    [ExecuteInEditMode]
    public class SphereHash : MonoBehaviour
    {
        public Vector3 position;
        public float radius;
        public static SphereHash<ushort> test = new SphereHash<ushort>(.5f);

        private List<Sphere> spheres = new List<Sphere>();

        private void Update()
        {
            if (test.Count == 0)
            {
                spheres.Clear();

                for (int i = 0; i < 16; ++i)
                {
                    var sphere = new Sphere
                    {
                        position = new Vector3((Random.value - 0.5f) * 5f,
                            (Random.value - 0.5f) * 5f,
                            (Random.value - 0.5f) * 5f),
                        radius = Random.value * 0.1f + 0.1f,
                    };

                    spheres.Add(sphere);
                    test.Add((ushort)i, sphere.position, sphere.radius);
                }
            }

            //test.Clear();
            //test.Add(0, position, radius);
        }

        private void OnDrawGizmosSelected()
        {
            test.DrawGizmos();

            Gizmos.color = Color.cyan;
            Gizmos.DrawSphere(position, radius);
        }
    }

    public class SphereHash<TData>
    {
        public struct Sphere
        {
            public Vector3 position;
            public float radius;
            public TData data;
        }

        public class Cell
        {
            public IntVector3 coordinates;
            public List<Sphere> elements = new List<Sphere>();
        }

        private Dictionary<IntVector3, Cell> cells 
            = new Dictionary<IntVector3, Cell>();

        public float CellSize;
        public int Count => cells.Count;

        public SphereHash(float cellSize)
        {
            CellSize = cellSize;
        }

        public void Clear(bool lazy = true)
        {
            if (lazy)
            {
                foreach (var cell in cells)
                {
                    cell.Value.elements.Clear();
                }
            }
            else
            {
                cells.Clear();
            }
        }

        public void Add(TData data, 
            Vector3 position, 
            float radius,
            float multiplier = 1f) // increase the bounding box of the sphere so we can update less frequently
        {
            float inv = 1f / CellSize;

            Sphere sphere = default(Sphere);
            sphere.position = FasterMath.Mul(position, inv);
            sphere.radius = radius * inv;
            sphere.data = data;

            Cell cell;
            var pos = default(IntVector3);

            int min_x = Mathf.FloorToInt(sphere.position.x - sphere.radius * multiplier);
            int min_y = Mathf.FloorToInt(sphere.position.y - sphere.radius * multiplier);
            int min_z = Mathf.FloorToInt(sphere.position.z - sphere.radius * multiplier);

            int max_x = Mathf.FloorToInt(sphere.position.x + sphere.radius * multiplier);
            int max_y = Mathf.FloorToInt(sphere.position.y + sphere.radius * multiplier);
            int max_z = Mathf.FloorToInt(sphere.position.z + sphere.radius * multiplier);

            for (pos.z = min_z; pos.z <= max_z; ++pos.z)
            {
                for (pos.y = min_y; pos.y <= max_y; ++pos.y)
                {
                    for (pos.x = min_x; pos.x <= max_x; ++pos.x)
                    {
                        if (!cells.TryGetValue(pos, out cell))
                        {
                            cell = new Cell { coordinates = pos, elements = new List<Sphere>() };
                            cells[pos] = cell;
                        }

                        cell.elements.Add(sphere);
                    }
                }
            }
        }

        public void AllTouchingSphere(Vector3 position, float radius, HashSet<TData> hits)
        {
            Cell cell;
            var pos = default(IntVector3);

            int min_x = Mathf.FloorToInt(position.x - radius);
            int min_y = Mathf.FloorToInt(position.y - radius);
            int min_z = Mathf.FloorToInt(position.z - radius);

            int max_x = Mathf.FloorToInt(position.x + radius);
            int max_y = Mathf.FloorToInt(position.y + radius);
            int max_z = Mathf.FloorToInt(position.z + radius);

            for (pos.z = min_z; pos.z <= max_z; ++pos.z)
            {
                for (pos.y = min_y; pos.y <= max_y; ++pos.y)
                {
                    for (pos.x = min_x; pos.x <= max_x; ++pos.x)
                    {
                        if (cells.TryGetValue(pos, out cell))
                        {
                            for (int i = 0; i < cell.elements.Count; ++i)
                            {
                                Sphere sphere = cell.elements[i];

                                Vector3 seperation = FasterMath.Sub(sphere.position, position);
                                float limitSqr = radius * radius + sphere.radius * sphere.radius;

                                if (seperation.sqrMagnitude <= limitSqr)
                                {
                                    hits.Add(sphere.data);
                                }
                            }
                        }
                    }
                }
            }
        }

        public bool NearestInsideSphere(Vector3 position, 
            float radius, 
            out TData nearest)
        {
            nearest = default(TData);
            float bestSqrDistance = radius * radius;
            bool success = false;

            Cell cell;
            var pos = default(IntVector3);

            int min_x = Mathf.FloorToInt(position.x - radius);
            int min_y = Mathf.FloorToInt(position.y - radius);
            int min_z = Mathf.FloorToInt(position.z - radius);

            int max_x = Mathf.FloorToInt(position.x + radius);
            int max_y = Mathf.FloorToInt(position.y + radius);
            int max_z = Mathf.FloorToInt(position.z + radius);

            for (pos.z = min_z; pos.z <= max_z; ++pos.z)
            {
                for (pos.y = min_y; pos.y <= max_y; ++pos.y)
                {
                    for (pos.x = min_x; pos.x <= max_x; ++pos.x)
                    {
                        if (cells.TryGetValue(pos, out cell))
                        {
                            for (int i = 0; i < cell.elements.Count; ++i)
                            {
                                Sphere sphere = cell.elements[i];
                                Vector3 seperation = FasterMath.Sub(sphere.position, position);
                                float sqrDistance = seperation.sqrMagnitude;

                                if (sqrDistance < bestSqrDistance)
                                {
                                    nearest = sphere.data;
                                    bestSqrDistance = sqrDistance;
                                    success = true;
                                }
                            }
                        }
                    }
                }
            }

            return success;
        }

        public void DrawGizmos()
        {
            float count = 0;
            float sum = 0;

            foreach (Cell cell in cells.Values)
            {
                Vector3 center = FasterMath.Mul((Vector3) cell.coordinates + Vector3.one * 0.5f, CellSize);

                Gizmos.color = Color.red;
                Gizmos.DrawWireCube(center, Vector3.one * CellSize);

                Gizmos.color = Color.green;
                foreach (Sphere sphere in cell.elements)
                {
                    Vector3 position = FasterMath.Mul(sphere.position, CellSize);

                    Gizmos.DrawLine(center, position);
                    Gizmos.DrawSphere(position, sphere.radius * CellSize);
                }

                if (cell.elements.Count > 0)
                {
                    count += 1;
                    sum += cell.elements.Count;
                }
            }
        }

        public void PrintMetrics()
        {
            float count = 0;
            float sum = 0;

            foreach (Cell cell in cells.Values)
            {
                if (cell.elements.Count > 0)
                {
                    count += 1;
                    sum += cell.elements.Count;
                }
            }

            Debug.Log($"{count} non-empty cells, with a mean of {count / sum} spheres each");
        }
    }
}