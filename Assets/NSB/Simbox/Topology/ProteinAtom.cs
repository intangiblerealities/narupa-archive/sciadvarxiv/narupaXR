﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace NSB.Simbox.Topology
{
    /// <summary>
    ///     Represents an atom in a protein.
    /// </summary>
    public class ProteinAtom : TopologyAtom
    {
        public AtomGroup Chain;
        public string ChainId;
        public AtomGroup Residue;
        public int ResidueId;
        public string ResidueName;

        public ProteinAtom(AtomGroup parent, string address, int index, string name, AtomGroup chain, AtomGroup residue)
            : base(parent, address, index, name)
        {
            Chain = chain;
            Residue = residue;
            ResidueName = residue.Name;
            ResidueId = residue.Id;
            ChainId = chain.Name;
        }
    }
}