﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace NSB.Simbox.Topology
{
    public class TopologyAtom
    {
        /// <summary>
        ///     Address of this atom.
        /// </summary>
        public string Address;

        /// <summary>
        ///     The absolute index of the atom in the system.
        /// </summary>
        /// <remarks>
        ///     This may differ from the atoms apparent index in rendering, depending on what is visible.
        /// </remarks>
        public int Index;

        /// <summary>
        ///     The name of the atom.
        /// </summary>
        public string Name;

        /// <summary>
        ///     The immediate parent of this atom.
        /// </summary>
        public AtomGroup Parent;

        public TopologyAtom()
        {
        }

        /// <summary>
        ///     Constructs a topology atom.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="address"></param>
        /// <param name="index"></param>
        /// <param name="name"></param>
        public TopologyAtom(AtomGroup parent, string address, int index, string name = "")
        {
            Parent = parent;
            Address = address;
            Index = index;
            Name = name;
        }
    }
}