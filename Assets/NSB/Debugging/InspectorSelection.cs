﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using NSB.MMD.RenderingTypes;
using NSB.Processing;
using UnityEngine;

namespace NSB.Debugging
{
    /// <summary>
    /// Debug component for modifying selections through the Unity inspector. 
    /// See InspectorSelectionEditor.
    /// </summary>
    public class InspectorSelection : MonoBehaviour, ISelectionSource
    {
        public FrameSource FrameSource;
        public SelectionSource SelectionSource;
        [SerializeField]
        private bool invertSource;

        NSBSelection ISelectionSource.Selection => Selection;
        public NSBSelection Selection = new NSBSelection();

        private void Update()
        {
            Selection.SetSourceAtomCount(FrameSource.Component.Frame.AtomCount);

            if (SelectionSource.Linked)
            {
                Selection.Clear();
                Selection.AddRange(SelectionSource.Component.Selection);

                if (invertSource)
                {
                    Selection.Invert();
                }
            }
        }
    }
}
