﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Rug.Osc;
using UnityEngine;


namespace Narupa.Client.Network
{
    public partial class NarupaServerConnection
    {
        //address for topology messages
        private readonly string topologyAddress = "/sim";

        // groups (from osc)
        private static Regex addressPattern = new Regex(@"\/sim\/(\w+)/(\d+)/(\w+)/[^\d]*(\d+)");

        
        public void SubscribeMessage(string address, Action<string, object[]> callback) =>
            client.SubscribeMessage(address, callback);

        public void UnsubscribeMessage(string address, Action<string, object[]> callback)=>
            client.UnsubscribeMessage(address, callback);

        public void SendMessage(string address, params object[] parameters) => client.SendMessage(address, parameters);

        private void OnNetworkMessage(string address, params object[] parameters)
        {
            try
            {
                if (address.StartsWith(topologyAddress + "/"))
                {
                    var match = addressPattern.Match(address);
                    if (match.Success)
                    {
                        string group = match.Groups[3].Value;
                        ushort id = (ushort) (int) parameters[0];
                        AddAtomToGroup(group, id);
                    }

                    Topology.AddAddress(address, parameters);
                }
                else if (address.StartsWith("/server/name"))
                {
                    SimulationNameRecieved?.Invoke((string) parameters[0]);
                }
                else if (address.StartsWith("/server/demo"))
                {
                    OnNewSimulationAvailable((string) parameters[0]);
                }
                else if (address.StartsWith("/mdat/endMetadata"))
                {
                    Topology.FinalizeTopology();
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }

        public void SendMultiplayerBoxTRS(Matrix4x4 matrix)
        {
            client.Send(new OscMessage(boxTRSAddress, Enumerable.Range(0, 16).Select(i => matrix[i]).Cast<object>().ToArray()));
        }

        public void SendPlay()
        {
            client.SendPlay();
        }

        public void SendPause()
        {
            client.SendPause();
        }

        public void SendStep()
        {
            client.SendStep();
        }

        public void SendReset()
        {
            client.SendRestart();
        }

        public void AttachOsc(string address, OscMessageEvent evnt)
        {
            client.TransportContext.OscManager.Attach(address, evnt);
        }
    }
}