﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Narupa.VR.Selection;
using Rug.Osc;
using UnityEngine;


namespace Narupa.Client.Network
{
    public partial class NarupaServerConnection
    {
        private string oscAddress = "/restraints";

        public void AddRestraints(InteractiveSelection selection, float restraintForce)
        {
            string address = oscAddress + "/addRestraints";

            int[] atoms = selection.Selection.ToArray();
            string name = selection.Name;

            List<object> payload = new List<object>();
            //Send message in chunks as there may be a lot of atoms. 
            int atomIndex = 0;
            int chunksize = 20;
            while (atomIndex < atoms.Length)
            {
                payload.Clear();
                payload.Add(name);
                payload.Add(restraintForce);
                for (int j = 0; j < chunksize && atomIndex < atoms.Length; j++)
                {
                    payload.Add(atoms[atomIndex]);
                    atomIndex++;
                }

                OscMessage message;
                try
                {
                    message = new OscMessage(address, payload.ToArray());
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                    return;
                }

                client.Send(message);
            }
        }

        public void RemoveRestraints(InteractiveSelection selection)
        {
            if (selection == null)
                return;
            string address = oscAddress + "/removeRestraints";
            string name = selection.name;

            OscMessage message = new OscMessage(address, name);
            client.Send(message);
        }
    }
}