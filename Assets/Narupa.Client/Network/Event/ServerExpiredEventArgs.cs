// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Client;

namespace Narupa.Client.Network.Event
{
    public class ServerExpiredEventArgs : ServerConnectionInfoEventArgs
    {
        public ServerExpiredEventArgs(SimboxConnectionInfo connection) : base(connection)
        {
        }
    }
}