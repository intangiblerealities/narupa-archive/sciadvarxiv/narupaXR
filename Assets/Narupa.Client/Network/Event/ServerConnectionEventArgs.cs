// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;

namespace Narupa.Client.Network.Event
{
    public class ServerConnectionEventArgs : EventArgs
    {
        public ServerConnectionEventArgs(IServerConnection connection)
        {
            ServerConnection = connection;
        }

        public IServerConnection ServerConnection { get; }
    }
}