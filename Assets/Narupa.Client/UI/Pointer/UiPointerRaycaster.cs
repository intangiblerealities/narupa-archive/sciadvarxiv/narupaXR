﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Narupa.Client.UI.Pointer
{
    /// <summary>
    ///     Override of the standard Unity raycaster for UI's. Based on the one provided in VRTK, this is required as the
    ///     standard Unity UI GraphicRaycaster has code to ignore UI's which are not visible on screen.
    /// </summary>
    public class UiPointerRaycaster : GraphicRaycaster
    {
        private Canvas currentCanvas;
        private Vector2 lastKnownPosition;
        private const float UI_CONTROL_OFFSET = 0.00001f;

        [NonSerialized]
        // Use a static to prevent list reallocation. We only need one of these globally (single main thread), and only to hold temporary data
        private static List<RaycastResult> raycastResults = new List<RaycastResult>();

        public override void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList)
        {
            if (Canvas == null)
                return;

            var ray = new Ray(eventData.pointerCurrentRaycast.worldPosition,
                eventData.pointerCurrentRaycast.worldNormal);
            Raycast(Canvas, eventCamera, ray, ref raycastResults);
            SetNearestRaycast(ref eventData, ref resultAppendList, ref raycastResults);
            raycastResults.Clear();
        }

        private void SetNearestRaycast(ref PointerEventData eventData,
            ref List<RaycastResult> resultAppendList, ref List<RaycastResult> raycastResults)
        {
            RaycastResult? nearestRaycast = null;
            foreach (var raycastResult in raycastResults)
            {
                var castResult = raycastResult;
                castResult.index = resultAppendList.Count;
                if (!nearestRaycast.HasValue || castResult.distance < nearestRaycast.Value.distance)
                {
                    nearestRaycast = castResult;
                }

                resultAppendList.Add(castResult);
            }

            if (nearestRaycast.HasValue)
            {
                eventData.position = nearestRaycast.Value.screenPosition;
                eventData.delta = eventData.position - lastKnownPosition;
                lastKnownPosition = eventData.position;
                eventData.pointerCurrentRaycast = nearestRaycast.Value;
            }
        }

        private float GetHitDistance(Ray ray)
        {
            var hitDistance = float.MaxValue;

            if (Canvas.renderMode != RenderMode.ScreenSpaceOverlay && blockingObjects != BlockingObjects.None)
            {
                var maxDistance = Vector3.Distance(ray.origin, Canvas.transform.position);

                if (blockingObjects == BlockingObjects.ThreeD || blockingObjects == BlockingObjects.All)
                {
                    RaycastHit hit;
                    Physics.Raycast(ray, out hit, maxDistance);
                    if (hit.collider)
                    {
                        hitDistance = hit.distance;
                    }
                }

                if (blockingObjects == BlockingObjects.TwoD || blockingObjects == BlockingObjects.All)
                {
                    var hit = Physics2D.Raycast(ray.origin, ray.direction, maxDistance);

                    if (hit.collider != null)
                    {
                        hitDistance = hit.fraction * maxDistance;
                    }
                }
            }

            return hitDistance;
        }

        private void Raycast(Canvas canvas, Camera eventCamera, Ray ray, ref List<RaycastResult> results)
        {
            var hitDistance = GetHitDistance(ray);
            var canvasGraphics = GraphicRegistry.GetGraphicsForCanvas(canvas);
            foreach (var graphic in canvasGraphics.ToList())
            {
                if (graphic.depth == -1 || !graphic.raycastTarget)
                {
                    continue;
                }

                var graphicTransform = graphic.transform;
                var graphicForward = graphicTransform.forward;
                var distance = Vector3.Dot(graphicForward, graphicTransform.position - ray.origin) /
                                 Vector3.Dot(graphicForward, ray.direction);

                if (distance < 0)
                {
                    continue;
                }

                //Prevents "flickering hover" on items near canvas center.
                if ((distance - UI_CONTROL_OFFSET) > hitDistance)
                {
                    continue;
                }

                var position = ray.GetPoint(distance);
                var camera = eventCamera;
                if (camera == null)
                    camera = Camera.current;
                Vector2 pointerPosition = camera.WorldToScreenPoint(position);

                if (!RectTransformUtility.RectangleContainsScreenPoint(graphic.rectTransform, pointerPosition,
                    eventCamera))
                {
                    continue;
                }

                if (graphic.Raycast(pointerPosition, eventCamera))
                {
                    var result = new RaycastResult()
                    {
                        gameObject = graphic.gameObject,
                        module = this,
                        distance = distance,
                        screenPosition = pointerPosition,
                        worldPosition = position,
                        depth = graphic.depth,
                        sortingLayer = canvas.sortingLayerID,
                        sortingOrder = canvas.sortingOrder,
                    };
                    results.Add(result);
                }
            }

            results.Sort((g1, g2) => g2.depth.CompareTo(g1.depth));
        }

        private Canvas Canvas
        {
            get
            {
                if (currentCanvas != null)
                {
                    return currentCanvas;
                }

                currentCanvas = gameObject.GetComponent<Canvas>();
                return currentCanvas;
            }
        }
    }
}