using System;
using UnityEngine;

namespace Narupa.Client.Config
{
    [Serializable]
    public abstract class NumericParameter<T> : Parameter<T>
    {
        [SerializeField] private T min;
        [SerializeField] private T max;

        public T Min => min;
        public T Max => max;

        protected NumericParameter(string name, T value, T min = default(T), T max = default(T)) : base(name, value)
        {
            this.min = min;
            this.max = max;
        }

        public virtual void CopyFrom(NumericParameter<T> parameter)
        {
            base.CopyFrom(parameter);
            min = parameter.Min;
            max = parameter.Max;
        }
        
        
        public void SetRange(T min, T max)
        {
            this.min = min;
            this.max = max;
        }
    }
}