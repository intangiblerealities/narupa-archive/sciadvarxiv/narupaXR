// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Science;
using Narupa.Client.Render.Definition;
using UnityEngine;

namespace Narupa.Client.Render.Colour
{
    /// <inheritdoc />
    /// <summary>
    ///     Colors each atom by their CPK color scheme
    /// </summary>
    public class CpkColorRenderer : ElementColorRenderer
    {
        [SerializeField] private AtomColorDictionary dictionary;

        private Color[] colors = new Color[256];

        private void Awake()
        {
            for (int i = 0; i < 256; i++)
                colors[i] = dictionary.Dictionary.ContainsKey(i) ? dictionary.Dictionary[i] : dictionary.DefaultColor;
        }

        /// <inheritdoc />
        protected override Color GetElementColor(ushort element)
        {
            return colors[element];
        }
    }
}