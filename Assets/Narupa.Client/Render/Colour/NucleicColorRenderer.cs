// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using NSB.Simbox.Topology;
using UnityEngine;

namespace Narupa.Client.Render.Colour
{
    /// <inheritdoc />
    /// <summary>
    ///     Renderer which colors DNA according to the DNA base
    /// </summary>
    public class NucleicColorRenderer : TopologyInfoRenderer
    {
        /// <inheritdoc />
        protected override Color GetAtomColor(TopologyAtom atom)
        {
            var atomId = atom.Name.Split('-')[0];
            if (atomId.Contains("'"))
                return Color.yellow;
            var resId = atom.Parent.Name.Split('-')[0];
            if (string.IsNullOrEmpty(resId))
                return Color.gray;
            var letter = resId[1];
            switch (letter)
            {
                case 'T':
                    return new Color32(160, 255, 160, 255);
                case 'A':
                    return new Color32(160, 160, 255, 255);
                case 'C':
                    return new Color32(255, 140, 75, 255);
                case 'G':
                    return new Color32(255, 112, 112, 255);
                case 'U':
                    return new Color32(160, 255, 160, 255);
                default:
                    return Color.gray;
            }
        }
    }
}