using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Narupa.Client.Render.Definition
{
    /// <inheritdoc />
    /// <summary>
    ///     Stores key-value pair of atomic number and color, for defining CPK style coloring
    /// </summary>
    [CreateAssetMenu(menuName = "Definition/Atom-Color Dictionary")]
    public class AtomColorDictionary : ScriptableObject
    {
        [Serializable]
        public class AtomColorKeyValuePair
        {
            public int AtomicNumber;
            public Color Color;
        }

        [SerializeField] private List<AtomColorKeyValuePair> dictionary;
        [SerializeField] private Color defaultColor;
        public Dictionary<int, Color> Dictionary => dictionary.ToDictionary(t => t.AtomicNumber, t => t.Color);
        public Color DefaultColor => defaultColor;
    }
}