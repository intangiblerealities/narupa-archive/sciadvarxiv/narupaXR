// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using System.Diagnostics.PerformanceData;
using System.Linq;
using Narupa.Client.Render.Context;
using Narupa.Client.Render.Property;
using UnityEngine;

namespace Narupa.Client.Render.Renderer
{
    /// <inheritdoc />
    /// <summary>
    ///     Renderer which draws a mesh in every cycle defined on the RenderContext
    /// </summary>
    public class CycleFillRenderer : RendererBehaviour, ITopologyUpdatedHandler
    {
        [RenderProperty("VERTEX.POSITION")] private IndexedProperty<Vector3> atomPosition;

        [RenderProperty("VERTEX.COLOR", ReadOnly = true)] private IndexedProperty<Color> atomColor;

        [RenderProperty("CYCLES")] private IndexedProperty<Cycle> cycles;

        [SerializeField] private MeshFilter meshFilter;

        private void Awake()
        {
            if (meshFilter == null)
                meshFilter = GetComponent<MeshFilter>();
        }

        /// <summary>
        ///     Updates the mesh of each ring
        /// </summary>
        private void PrepareMesh()
        {
            var mesh = meshFilter.mesh;
            if (mesh == null)
                meshFilter.mesh = new Mesh();

            vertices.Clear();
            var triangles = new List<int>();
            var colors = new List<Color>();
            var off = 0;
            foreach (var ring in cycles)
            {
                var ringSize = ring.Count;
                var pos = ring.Select(index => atomPosition[index]).ToArray();
                vertices.AddRange(pos);
                for (var i = 2; i < ringSize; i++)
                    triangles.AddRange(new[] {off, off + i - 1, off + i});
                Vector4 c = Vector3.zero;
                foreach (var i in ring)
                    c += (Vector4) atomColor[i];
                c /= ring.Count;
                foreach(var i in ring)
                    colors.Add(c);
                off += ringSize;
            }
            mesh.SetVertices(vertices);
            mesh.SetTriangles(triangles, 0);
           
            mesh.SetColors(colors);
            mesh.UploadMeshData(false);
        }

        private readonly List<Vector3> vertices = new List<Vector3>();
        
        /// <summary>
        ///     Updates the mesh of each ring
        /// </summary>
        private void UpdateMesh()
        {
            var mesh = meshFilter.mesh;
            
            var i = 0;
            foreach (var ring in cycles)
            {
                foreach (var k in ring)
                {
                    vertices[i] = atomPosition[k];
                    i++;
                }
            }

            mesh.SetVertices(vertices);
            mesh.UploadMeshData(false);
        }

        /// <inheritdoc />
        public override void Refresh()
        {
            UpdateMesh();
        }

        public void TopologyUpdated()
        {
            PrepareMesh();
        }
    }
}