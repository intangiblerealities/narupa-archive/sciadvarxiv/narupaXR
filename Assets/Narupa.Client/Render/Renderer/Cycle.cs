// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;

namespace Narupa.Client.Render.Renderer
{
    /// <summary>
    ///     Represents a cycle in a graph
    /// </summary>
    public class Cycle : List<int>
    {
        public Cycle(IEnumerable<int> ints)
        {
            AddRange(ints);
        }
    }
}