// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections;
using System.Collections.Generic;
using NSB.Utility;

namespace Narupa.Client.Render.Property
{
    /// <inheritdoc />
    /// <summary>
    ///     Default wrapper for an IList which implements IIndexed. Using Set(), the underlying collection can be
    ///     changed without needing to update references.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class IndexedCollection<T> : IIndexed<T>
    {
        private IList<T> collection;

        public IndexedCollection(IList<T> collection)
        {
            this.collection = collection;
        }

        public IndexedCollection(int size) : this(new List<T>(size))
        {
        }

        public T this[int i]
        {
            get { return collection[i]; }
            set { collection[i] = value; }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return collection.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int Count => collection.Count;

        /// <summary>
        ///     Updated the collection which is contained in this collection.
        /// </summary>
        /// <param name="newCollection"></param>
        public void Set(IList<T> newCollection)
        {
            collection = newCollection;
        }

        public void Resize(int count)
        {
            var list = collection as List<T>;
            if(list != null)
                list.Resize(count);
            else
                collection = new T[count];
        }
    }
}