using Narupa.Client.Render.Context;
using Narupa.Client.Render.Property;
using Narupa.Client.Render.Renderer;
using UnityEngine;

namespace Narupa.Client.Render.Radius
{
    /// <inheritdoc />
    /// <summary>
    ///     General base class for a renderer which provides a radius
    /// </summary>
    public class RadiusRenderer : RendererBehaviour, ITopologyUpdatedHandler
    {
        [RenderProperty("VERTEX.RADIUS", WriteOnly = true)]
        private IndexedProperty<float> atomRadius;

        /// <summary>
        ///     The default color to be returned when it cannot be determined for the color-choosing method
        /// </summary>
        /// <returns></returns>
        protected virtual Color GetDefaultColor()
        {
            return Color.grey;
        }

        protected virtual void RefreshRadii(IndexedCollection<float> target)
        {
            
        }

        private IndexedCollection<float> cachedRadii = new IndexedCollection<float>(0);

        public override void Refresh() {
            atomRadius.Set(cachedRadii);
        }
        

        public void TopologyUpdated()
        {
            RefreshRadii(cachedRadii);
            atomRadius.Set(cachedRadii);
        }
    }
}